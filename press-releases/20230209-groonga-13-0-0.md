---
layout: default
main_title: 国産高速全文検索エンジンGroonga 13.0.0をリリース
sub_title:
type: press
---

<p class="press-release-subtitle-small">
日本語のWebサイト内検索・ECサイト内検索ならGroongaが最適！
</p>

<div class="press-release-signature">
  <p class="date">2023年2月9日</p>
  <p>株式会社クリアコード</p>
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平　以下、クリアコード）は、国産の高速全文検索エンジン「Groonga」の最新版Groonga 13.0.0をリリースしたことをお知らせします。今回のメジャーバージョンアップと関連プロジェクトの最新情報は、公式サイトで公開しています。


* Groonga最新情報: https://groonga.org/ja/blog/2023/02/09/groonga-13.0.0.html



## 国産高速全文検索エンジンGroongaについて

[Groonga（ぐるんが）](https://groonga.org/ja/)は、日本人が開発したオープンソースの高速かつ高精度な全文検索エンジンです。使い方次第で様々な用途での利用が可能になるように、Groongaの機能をPostgreSQLやMySQL、MariaDBなどのメジャーなデータベースから使用できる関連プロジェクトもあります。
リリースから10年以上たった今でも、定期的にリリースされ、利用者のニーズに合わせた新機能の追加や、バグの修正、最新のプラットフォームへの対応など活発に開発が続いています。

Groongaのマネージドサービスがないことで使いにくいケースがありましたが、2022年12月からはマネージドサービスとしてGroongaを使えるようになりました。マネージドPostgreSQLを含むアプリケーション開発プラットフォームを提供する『[Supabase](https://supabase.com/)』がPGroongaをサポートしました。PGroongaによりPostgreSQL経由でGroongaを使えます。


### Groongaの強み

#### 高性能なマルチバイト文字対応検索

Groongaは主に日本人が開発していることもあり日本語をはじめとしたマルチバイト文字対応が強力です。きめ細やかなテキスト正規化やローマ字で入力された検索キーワードでもオートコンプリートできるなど日本語Webサイト内検索・ECサイト内検索に必要な機能が豊富に揃っています。

#### 優れた更新性能

Groongaは更新性能も優れています。登録されたデータをリアルタイムで検索結果に反映します。定期的に反映するのではなく、すぐに反映します。これは情報の鮮度が重要なECサイト検索で非常に重要です。また、更新頻度が高いECサイトでも優れた性能を発揮します。さらに更新時も検索をブロックしないためどんどん新しい情報を反映しても検索性能が落ちません。常にデータを最新情報に更新してユーザーに有用な情報を高速に提供することができます。

#### 多様な利用方法

Groongaは単独で全文検索HTTPサーバーとして利用することもできますし、PostgreSQL/MySQL/MariaDB/Percona Server for MySQLに組み込んで利用することもできます。単独で利用する場合はHTTP、組み込んで利用する場合はSQLと広く使われている方法で利用できるため簡単に使えます。

PostgreSQLに組み込む場合は[PGroonga（ぴーじーるんが）](https://pgroonga.github.io/ja/)を、MySQL/MariaDB/Percona Server for MySQLに組み込む場合は[Mroonga（むるんが）](https://mroonga.org/ja/)を使います。


### YouTubeチャンネルのご案内

Groongaでは、YouTubeチャンネルを開設しています。
YouTubeチャンネルでは、2022年から毎週火曜日にYouTube Liveを開催しています。


YouTube Liveの内容は、毎月リリースされているGroongaの最新情報を簡単に楽しくキャッチアップできるよう開発者が最新のリリースを自慢する「リリース自慢会」と、Groongaのわかりづらい部分や、仕組みの基礎などを開発者に直接質問する「グルカイ！（Groonga開発者に聞け！）」があります。
YouTube Liveを開催しない場合や急遽中止する場合はTwitterなどでお知らせします。


コメントも開放しているので開発者に直接質問することもできます。Groongaを使い始めたら必見です。YouTubeのチャンネル登録をしておけば配信日が通知され見逃すことはありません。またイベント開催のお知らせページもありますのでこちらでもメンバー登録いただけると情報が届きます。

* Groonga YouTubeチャンネル： https://www.youtube.com/channel/UC9XcXAhHSb2TMPqlCvUHHbQ
* Groonga Connpassページ: https://groonga.connpass.com/

### Groongaコミュニティーのご案内

Groongaプロジェクトはリリース情報やリリース自慢会の開催情報をTwitterやFacebookなどで発信しています。また、うまい使い方がわからない、困ったことがあるというときは、チャットや掲示板で開発者・ユーザー間で情報交換することもできます。

Groongaを使い始めたらまずはTwitterとYouTubeをフォローするのがおすすめです。困ったことがあったらチャットや掲示板などを活用してください。

* Twitter： https://twitter.com/groonga
* Facebook: https://www.facebook.com/groonga/
* YouTube: https://www.youtube.com/channel/UC9XcXAhHSb2TMPqlCvUHHbQ
* Gitter（チャット）： https://gitter.im/groonga/ja
* GitHub Discussions（掲示板）： https://github.com/groonga/groonga/discussions


## Groongaサポートサービスについて

クリアコードは10年以上にわたりGroongaプロジェクトのメンバーとして、Groonga本体および関連プロジェクトの開発、リリース、コミュニティーでのサポートをしてきました。

この経験を活かしてGroonga/Mroonga/PGroonga導入企業の有償サポートサービスを提供しています。トラブル時の調査はもちろん性能を引き出すための使い方のコンサルティングやGroonga/Mroonga/PGroongaの改良による問題解決など開発者だからこそできる深いサポートのため安心して使うことができます。

* Groongaサポートサービス： {{ site.url }}{% link services/groonga.md %}



## クリアコードについて

クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。

Fluentd/Fluent Bit/Apache Arrow/Groonga/組み込みシステム向け日本語入力ソフトウェアなど、多岐にわたるソフトウェアの開発やソースコードレベルでの技術支援を行っています。

クリアコードの理念は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の理念です。

### 参考URL

【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20230209-groonga-13-0-0.md %}]({% link press-releases/20230209-groonga-13-0-0.md %})

【関連サービス】[{{ site.url }}{% link services/groonga.md %}]({% link services/groonga.md %})

【プロジェクトページ】
* [Groonga](https://groonga.org/ja/)
* [Mroonga](https://mroonga.org/ja/)
* [PGroonga](https://pgroonga.github.io/ja/)
* [Rroonga](http://ranguba.org/ja/#about-rroonga)


### 当リリースに関するお問合せ先

株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com

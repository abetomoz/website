---
date: 2021-02-09
title: IEView WE v1.6.3
layout: default
tags:
  - IEView WE
---

## {{page.title}}

{{page.title}}は、{{page.date | date: "%Y年%-m月%-d日" }}に公開されたバージョンです。

### 修正された不具合

 * 仮想環境でThinBridgeと連携した場合に、管理者設定が正常に読み込まれない不具合を修正しました。

---
date: 2020-11-16
title: BrowserSelector v2.0.0
layout: default
tags:
  - BrowserSelector
---

## {{page.title}}

{{page.title}}は、{{page.date | date: "%Y年%-m月%-d日" }}に公開されたバージョンです。

### グループポリシーによる集中管理に対応しました

本バージョンから、インストーラにADMX形式のテンプレートが付属するようになっています。
以下のように、INI形式の設定ファイルに代えて、グループポリシーで設定を集中管理することができます。

![A screenshot of gpedit.exe]({% link _support/20201116/policy.png %})

具体的な利用方法は、BrowserSelector利用ガイドの「4.3. グループポリシーで設定を集中管理する」を参照下さい。

### FirefoxCommandに環境変数を利用できるようになりました

以下のように、Firefoxの起動パス指定に環境変数を利用できるようになりました。

Firefoxの起動パスが環境によって変わる場合（例えば、ユーザーデータ領域にインストールしている場合など）を想定した機能です。

```ini
FirefoxCommand=%AppData%\Mozilla Firefox\firefox.exe
```

### その他の新機能

 - FirefoxからGoogle Chromeへのリダイレクトに対応しました。

 - 64ビット版Internet Explorerのサポートを追加しました。

### 修正された不具合

 - IEの信頼済みサイトのリストを設定した場合に、登録対象サイトについて、
   IEからのリダイレクトが不安定になる問題を修正しました。

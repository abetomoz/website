---
layout: default
main_title: 採用情報
sub_title:
type: recruit
description: クリアコードではソフトウェア開発者を採用しています。Apache Arrow・Groonga・Fluentdなど各種自由なソフトウェアの設計・開発・サポートが業務内容なので業務の成果を公開したり世界中の開発者と共同開発したり成果を対外発信したりできます。フルリモートでの勤務などライフスタイルに合わせて勤務できます。
---

## ソフトウェア開発者採用

クリアコードのミッションは、自由なソフトウェアとビジネスを両立させることです。<br>
この目標を実現するために、様々なソフトウェアの設計・開発・サポートに携わっていただきます。

クリアコードでソフトウェア開発者として働くと次のようなメリットがあります。

### 業務の成果を公開できる

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    クリアコードでは、業務の成果をできるだけ自由なソフトウェアとして広く公開しています。
    <p>
    開発の成果がオープンになることで、技術者としてのキャリア形成に繋がるのはもちろん、自由なライセンスで公開するために依存ライブラリにも自由なソフトウェアを使うので、その調査や改修を通じて、他の人が書いたコードから学ぶ機会も多いのが特長です。
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/twitter.png %}' alt='ClearCode develops XMLua'>
  </div>
</div>

### 世界中の開発者と共同開発できる

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    自由なソフトウェアに国境はありません。バックグラウンドの異なる世界中の開発者と一緒に開発することで、多様な考え方から新しい視点に気づいたり、共同開発の楽しさ・難しさを体験できたりします。
    <p>
    クリアコードは小さな会社ですが、グローバルな大きな会社で働くときのように多様な開発者と共同開発できます。
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/contribute.png %}' alt='A comment thread in bugzilla.mozilla.org'>
  </div>
</div>

### 業務の一環として対外発信できる

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    私たちは業務の成果を広く公開することで次の仕事につなげています。日々の業務で得られた知見をブログにまとめたりイベントで発表することは重要な業務の一つです。
    <p>
    ブログ執筆、イベントの準備から発表まで、業務として行うことができます。
  </div>
  <div class='cell large-shrink'>
    <iframe src="https://slide.rabbit-shocker.org/authors/kou/kansai-rubykaigi-2017/viewer.html"
        width="320" height="280"
        frameborder="0"
        marginwidth="0"
        marginheight="0"
        scrolling="no"
        style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
        allowfullscreen> </iframe>
    <div style="font-size:0.8em;margin-bottom: 5px">
      <a href="https://www.clear-code.com/blog/2017/5/29.html">関西Ruby会議2017基調講演</a>
    </div>
  </div>
</div>

### ライフスタイルにあわせて柔軟に働き方を選べる

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    結婚、出産、育児、介護などライフイベントにあわせて、様々な働き方を提案し、サポートします。
    <p>
    現時点でも育児休業、在宅勤務、フレックスタイム、短時間勤務といった制度が整っています。ライフスタイルが変わっても長くクリアコードで働けるように制度を整備し続けます。
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/lifestyle.jpg %}' alt='An image of work-life balance'>
  </div>
</div>

### 社員ブログ
クリアコードで現在働いている人が、どんな風に感じているかをブログで紹介しています。会社の雰囲気もわかると思うので興味があればぜひご覧ください。

* [「クリアコードで働く」私のビフォーアフター]({% post_url 2023-04-24-how-i-got-started-working-at-clear-code-yashiro %})
* [クリアコードに入社した理由と入社後の変化 - フリーソフトウェアについてはよく知らないけど「コードがたくさん書けそう」という理由で応募し、その後フリーソフトウェア開発のスタイルに慣れるまでの変化 -]({% post_url 2022-07-13-working-at-clear-code-horimoto %})
* [クリアコードの入社経緯と入社して思ったこと: 働きやすく、開かれた環境]({% post_url 2022-07-13-working-at-clear-code-hashida %})
* [オープンな開発をできる仕事 @ クリアコード]({% post_url 2022-07-08-work-in-clear-code-fukuda %})
* [クリアコードで働いて知ったことと思ったこと]({% post_url 2022-07-06-working-at-clear-code %})


## 業務の紹介

具体的な業務の事例を紹介します。

ここに掲載されている業務は一例に過ぎず、他にも随時開発案件などもあるため、どの業務に携わっていただくかはご本人のご希望と他プロジェクトの状態などを相談しながら決めていきます。

### 全文検索エンジンGroonga関連業務

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    有償のサポート・コンサルティングサービスを購入いただいているお客さんに対し、回答・提案・導入支援などを実施します。また、必要に応じてGroongaに対し機能追加・高速化・不具合修正なども実施します。
    <p>
    コミュニティーからの問い合わせ・機能追加要望・パッチのレビューなどに対応することも重要な業務です。ここからフリーソフトウェアの推進および案件獲得につながるからです。
    <p>
    その他にも、会社のブログやイベント発表など、業務で得た知見を公開し、Groongaを広める活動も実施します。これもフリーソフトウェアの推進および案件獲得のための活動です。
    <p>
    <a href="{% link recruitment/groonga.md %}">もっと読む</a>
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/groonga.png %}' alt='Groonga logo'>
  </div>
</div>

### Apache Arrow関連業務

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    クリアコードは2016年より継続的にApache Arrowの開発に参加しています。2020年8月24日時点で2番目に多くコミットした開発者（右図の緑枠）となっています。
    <p>
    Apache Arrowについてコンサルティング・ソースコードレベルのサポートを提供しており、以下の業務を行います。
    <ul>
    <li>自社のシステムをApache Arrowに対応させたい企業に対するコンサルティング
    <li>Apache Arrowを使った大規模データ処理に関する共同研究
    <li>データ処理における課題解決のためのApache Arrow活用支援とApache Arrowへの機能追加
    </ul>
    <a href="{% link recruitment/apache-arrow.md %}">もっと読む</a>
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/arrow.png %}' alt='Arrow commit ranking'>
  </div>
</div>

### データ収集ツールFluentd関連業務

<div class='grid-x grid-padding-x'>
  <div class='cell large-auto'>
    <p>
    クリアコードは2015年より継続的にFluentdの開発に参加しており、現在ではFluentdやtd-agentのリリースも行っています。Fluentdは広く普及しており、世界中の名だたる企業のエンジニアとやりとりする機会も多いため、技術者として大変やりがいのある仕事です。
    </p>
    <p>
    具体的な業務としては、有償のサポート・コンサルティングサービスを購入いただいているお客様に対し、回答・提案・導入支援などを実施します。また、必要に応じてFluentdや関連フリーソフトウェアに対する機能追加や不具合修正、新規プラグインの開発なども実施します。Fluentdプロジェクト全般の日々のメンテナンスやリリース作業、コミュニティー対応にも従事して頂きます。
    </p>
    <a href="{% link recruitment/fluentd.md %}">もっと読む</a>
  </div>
  <div class='cell large-shrink'>
    <img src='{% link recruitment/fluentd.png %}' alt='Fluentd logo'>
  </div>
</div>

## 募集要項 (ソフトウェア開発者)

<table class='offer'>
<tr>
  <th>業務内容</th>
  <td>
    <p>以下の自由なソフトウェアの開発・導入支援・コンサルティング・サポートに携わっていただきます。
    <ul>
    <li>全文検索エンジン<a href='https://groonga.org/'>Groonga</a>
    <li>ログ収集ツール<a href='https://www.fluentd.org/'>Fluentd</a>・<a href='https://fluentbit.io/'>Fluent Bit</a>
    <li>データ処理基盤<a href='https://arrow.apache.org/'>Apache Arrow</a>
    <li><a ref='{{ "/services/embedded-service.html" | relative_url }}'>組み込み機器ソフトウェア開発</a>
    <li><a href='https://www.mozilla.org/ja-JP/firefox/new/'>Firefox</a>・ブラウザ切替ツール<a href='{{ "/services/browserselector.html" | relative_url }}'>BrowserSelector</a>などのブラウザ関連
    <li><a href='https://www.thunderbird.net/ja/'>Thunderbird</a>・<a href='https://milter-manager.osdn.jp/index.html.ja'>milter manager</a>などのメール関連
    </ul>
    <p><b>主に利用している技術</b>
    <ul>
    <li>言語: C、C++、Ruby、JavaScript、Python
    <li>技術: Fluentd、Groonga、MySQL、PostgreSQL、Apache Arrow、Gecko
    <li>開発: Zulip（チャット）、GitLab（ソースコード管理）、Redmine（チケット管理）、Nextcloud（スケジュール管理）
    </ul>
    <p>※ 業務ツールには原則として自由なソフトウェアを採用しています。
  </td>
</tr>
<tr>
  <th>応募資格</th>
  <td>
    <p><b>求められる経験・資質</b>
    <ul>
    <li>プログラミングが楽しいこと
    <li>だれかと一緒に問題解決することが好きなこと
    </ul>
    <p><b>あると好ましい経験・資質</b>
    <ul>
    <li>自由なソフトウェアにコントリビューションした経験
    <li>クリアコードで利用している言語の経験（C、C++、Ruby、JavaScript、Python）
    <li>法人を顧客としてソフトウェアをサポートした経験
    <li>勉強会やカンファレンスでの登壇経験
    <li>英語の読み書きの能力
    </ul>
  </td>
</tr>
<tr>
  <th>エンジニアの働き方</th>
  <td>
    <ul>
    <li>本社は所沢にありますが、リモート勤務が前提になります。
    <li>プロダクトごとのチーム制で開発・サポートを行っています。
    <li>各チームで適宜打ち合わせを行います。また、毎週、自由参加の雑談会を設けています。そのほか働きやすくするための各種取り組みを行っています。
    <li>業務や技術に関連する知識について、得意な社員が講師役を務める社内勉強会も適宜開催されています。
    <li>アクティブ評価制度。数カ月ごとに社員それぞれと面談し、これからの活動方針のフォローをします。また評価制度自体を定期的に見直します。
    </ul>
  </td>
</tr>
<tr>
  <th>勤務形態</th>
  <td>正社員ないしパートタイム<br>
      ※正社員は10:00〜15:00をコアタイムとするフレックスタイム制を採用しています。</td>
</tr>
<tr>
  <th>給与</th>
  <td>
    <ul>
    <li>経験やスキルに応じて相談します（給与例: 入社4年、30歳、800万円）
    <li>人事評価に基づいて、毎年昇給の検討があります。
    <li>賞与は年一回です（会社業績に応じて支給額を検討します）
    </ul>
  </td>
</tr>
<tr>
  <th>休暇</th>
  <td>
    <ul>
    <li>週休2日・祝日・年末年始休暇・リフレッシュ休暇（年間3日）
    <li>育児休暇/介護休暇制度あり。2021年3月25日現在、過去3年間で延べ4名が育児休暇制度を利用。取得期間1ヶ月から6か月。
    <li>有給休暇の取得率は平均86%。(2023年8月31日時点)
    </ul>
  </td>
</tr>
<tr>
  <th>福利厚生</th>
  <td>
    <ul>
    <li>各種社会保険完備：厚生年金、健康保険(関東ITソフトウェア健康保険組合)、企業年金(日本ITソフトウェア企業年金基金)、雇用保険
    <li>退職金制度（中小企業退職金共済、東京商工会議所特定退職金共済）
    <li>社宅制度
    </ul>
  </td>
</tr>
<tr>
  <th>選考フロー</th>
  <td>
  <p>一般的なフローとは異なり、書類選考はありません。
  <ol>
  <li>問い合わせフォームより応募
  <li>会社説明会（※ <a href='{% post_url 2023-04-01-introducing-company-intoroduction-for-recruitment %}'>会社説明会についてはこちら</a>）
  <li>パッチ採用を実施 （※<a href='{{ "/recruitment/patch.html" | relative_url }}'>パッチ採用についてはこちら</a>）
  <li>内定
  </ol>
  <p>
  </td>
</tr>
</table>

<center>
<a class="btn" href="{{ "/contact/" | relative_url }}">説明会申し込み<i class="fas fa-angle-double-right"></i></a>
</center>

### 会社説明会申し込みのフォーム入力例

会社説明会の申し込み時には、次の例のように氏名・メールアドレス・希望日時の3点を記入してご連絡ください。

```
名字: oo
名前: xx
メールアドレス: my-address@example.com
種類: 会社説明会について
内容: ooと申します。次の日時で会社説明会をお願いします。

 * 3月15日 17:00〜
 * 3月16日 17:00〜
 * 3月22日 17:00〜
```

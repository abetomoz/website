---
tags:
- ruby
- presentation
title: 'RubyKaigi 2013にシルバースポンサーとして参加したまとめ #rubykaigi'
---
[シルバースポンサーとして応援](http://rubykaigi.org/2013/sponsors#Silver01)した[RubyKaigi 2013](http://rubykaigi.org/2013)が終了しました。今後、どこかのスポンサーの参考になるかもしれないので、クリアコードがスポンサーとして参加したRubyKaigi 2013をまとめます。
<!--more-->


### スポンサーの種類

クリアコードはRubyを応援しています。その一環として[日本Ruby会議2009](http://rubykaigi.org/2009/ja)から毎回スポンサーとしてRuby会議を応援してきました。

スポンサーには額に応じていくつか種類があるのですが、クリアコードは毎回一番低い額のスポンサー枠です。今回だとシルバースポンサーです。より額の多いスポンサー枠に比べれば微々たるものですが、身の丈にあった応援をしています。

少し話はずれて参加者としての視点になりますが、RubyKaigi 2013でのスペシャルスポンサーの各社のスポンサーシップは、直接的に助かるためスポンサーのありがたさがとてもよくわかりました。例えば、Microsoftさんのドリンク提供や、Herokuさんの弁当提供や、DeNAさんのWiFi提供などです[^0]。参加者としてとても助かりました。ありがとうございます。

### ノベルティ配布

会場にノベルティ置き場があるということだったので、次のものを配布しました。

  * [rroongaステッカー](http://groonga.org/ja/blog/2013/05/10/sticker.html): 5枚
  * [mroongaステッカー](http://groonga.org/ja/blog/2013/05/10/sticker.html): 5枚
  * [groongaステッカー](http://groonga.org/ja/blog/2013/05/10/sticker.html): 20枚 + 約80枚
  * [groongaサポートサービス](http://groonga.org/ja/support/)のチラシ: 20枚 + 10枚
  * [コミットへのコメントサービス](/services/commit-comment.html)のチラシ: 20枚 + 10枚
  * [インターンシップ](/internship/)応募者募集のチラシ: 20枚

以前、つくばで日本Ruby会議があったときもチラシを配布したのですが、そのときは多く持っていってしまって配布しきれませんでした。その経験をふまえて、今回は分相応の量を持っていきました。しかし、後述のとおり、予想以上に持っていく方が多かったため2日目に追加しました。「+ XX枚」となっているものが2日目に追加した分です。

これらの配布が終了するペースは次のとおりです。

  * rroongaステッカー: 初日の午前中で配布終了。
  * mroongaステッカー: 初日の午前中で配布終了。
  * groongaステッカー: 初日の午前中で20枚は配布終了。2日目に追加した約80枚は2日目が終わる頃には配布終了。
  * groongaサポートサービスのチラシ: 初日の夕方には20枚の配布は終了。2日目に追加した10枚は2日目が終わる頃には配布終了。
  * コミットへのコメントサービスのチラシ: 初日が終わる頃には20枚の配布は終了。2日目に追加した10枚は3日目の夕方には配布終了。
  * インターンシップ応募者募集のチラシ: どの日も5枚ずつくらい減っていて、最終的に7枚残った。

全体として、クリアコードの想像以上に興味のある人が多いということがわかりました。自分たちの製品やサービスなどにどのくらいの人が興味を持つかを観測するために、RubyKaigiでステッカーや関連資料を配布してみるのはよいかもしれません。観測するだけであれば、来場者数の1/10の数もあれば十分でしょう。今回は574名（スピーカーやスタッフを含む）の参加ということなので50部もあれば十分でしょう。

配布状況については以上です。

RubyKaigi 2013が終了して数日しか経っていませんが、現時点での配布後の状況にも少し触れます。インターンシップへの応募がありました[^1]。これは驚くべき成果です。今後、RubyKaigi効果での成果が増えたら報告します。

ただ、これはチラシを配布しただけの効果か、発表もしたからなのかはわかりません。なお、発表資料は次のとおりです。

  * Rubyリファレンスマニュアル刷新計画2013 初夏
    * [SlideShare](http://www.slideshare.net/okkez/rubykaigi2013)
    * [Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/okkez/rubykaigi2013/)
    * [Ustream](http://www.ustream.tv/recorded/33520887)（後半30分）
  * Be a library developer!
    * [SlideShare](http://www.slideshare.net/kou/rubykaigi-2013)
    * [Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2013/)
    * [Ustream](http://www.ustream.tv/recorded/33615831)

「Be a library developer!」は[RubyKaigi 2013前に紹介したときに内容を説明]({% post_url 2013-05-28-index %})してあります。参考にしてください。

### まとめ

シルバースポンサーとしてRubyKaigi 2013を応援した会社視点でRubyKaigi 2013での活動をまとめました。日本Ruby会議の頃はブースやスポンサーの発表枠などもありましたが、RubyKaigiではWebサイト掲載用のロゴと会社説明の送付だけになったのでスポンサーとしてやることはほとんどなくなりました。必要ならノベルティを配布する程度です。もし、Rubyを応援したくて多少金銭的な余裕があるのであれば、それほど手間がかからない[^2]のでRubyKaigiのスポンサーになってみるのはいかがでしょうか。

### おまけ：RubyHiroba 2013でリーダブルなコードを考える会をした

スポンサーとは関係ありませんが、RubyKaigi 2013関連としてRubyHiroba 2013にも少し触れます。

RubyHiroba 2013で「リーダブルなコードはどんなコードかを実際のコードを題材にして考える会」をしました。そもそも誰も参加しなくて開催すらできないのではないかと心配していましたが、5人で開催できました。

題材はRabbitというプレゼンツールのコードにしました。参加してくれたみなさんが積極的に考えを話してくれたので、いろんな視点でリーダブルなコードを考える機会になりました。この会をふりかってみると、進行役として話や注目している点をうまく整理する人がいると、「みんなで」考えることをよりうまくできそうでした。また機会があれば、そのあたりを試してみます。参加してくれたみなさん、ありがとうございました！

[^0]: RICHOさんの提供したサイネージはあまり使わなかったので触れていません。すみません。でも、楽しそうに絵を書いたりしている人たちを見たので、有効活用した人たちはたくさんいそうです。

[^1]: 応募があっただけで、まだ開始したりしているわけではありません。

[^2]: 効果を最大化したいとなると、そこそこ手間がかかるかもしれません。

---
tags:
- mozilla
- feedback
title: MozReviewでのFirefoxへのパッチ投稿方法
---
<ins>


（2019年4月12日追記）
この記事で解説しているMozReviewは既に*廃止*されており、[代わりにPhabricatorというツールを使うようになっています]({% post_url 2018-11-15-index %})。この記事自体は歴史的文書として残していますが、*パッチを新たに投稿したいという場合には[Phabricatorの解説]({% post_url 2018-11-15-index %})を参照して下さい*。
<!--more-->


</ins>


### はじめに

[前回の記事]({% post_url 2018-07-03-index %})ではFirefoxへのフィードバックの仕方を紹介しました。この記事で紹介したように、Firefoxへのパッチ投稿は、以前は[Bugzilla](https://bugzilla.mozilla.org)にパッチファイルを手動で添付する形で行われていました。その方法は今でも通用するのですが、最近ではMozReviewという[Review Board](https://www.reviewboard.org/)ベースのコードレビューシステムも導入されています。GitHubと同様にソースコードにインラインでコメントを付けられますし、パッチをリモートリポジトリでバージョン管理できるので、現在ではこちらを使用する方がおすすめです。しかし最初に少し設定が必要ですので、使い始めるまでに心理的障壁があるのも事実かと思います。そこで今回はMozReviewの使用方法について紹介します。

### セットアップ

本記事で紹介するセットアップ方法は[MozReview User Guide](https://mozilla-version-control-tools.readthedocs.io/en/latest/mozreview-user.html)を元にしています。詳細はそちらを参照して下さい。

#### 用意するもの

  * [BMO(bugzilla.mozilla.org)](https://bugzilla.mozilla.org)のアカウント

    * まだBMOアカウントを取得していない場合は https://bugzilla.mozilla.org/createaccount.cgi で作成します。

    * Real nameには`Your Name [:ircnick]`のような形で末尾にニックネームを付けておきましょう。

      * [Review Board](https://reviewboard.mozilla.org)上ではこのニックネームがユーザー名として表示されます。

      * ただし、既に存在するニックネームは使用できません（代わりにメールアドレスのアカウント名+任意の数字になります）。

  * mozilla-centralのワーキングコピー

    * [前回の記事]({% post_url 2018-07-03-index %})等を参考に、`hg clone https://hg.mozilla.org/mozilla-central`で取得して下さい。

    * 本記事ではバージョン管理システムとしてMercurialを使用する場合のみを対象とします

      * MozReviewはgitでも使用できるようですが、本記事では対象としていません。

#### Review Boardへのログイン確認

MozillaのReview Boardは https://reviewboard.mozilla.org でアクセスできます。Bugzillaアカウントの登録が終わったら、Review Boardにもログインできることを確認してみましょう。アカウントはBugzillaと連携されているため、[Review Boardのログインページ](https://reviewboard.mozilla.org/account/login)にアクセスすると、自動的にログインすることができます。

Review Boardにログインできたら、画面右上のユーザー名を確認して、先ほど設定したニックネームを使用出来ていることを確認すると良いでしょう。

![MozReviewユーザー名]({{ "/images/blog/20180705_0.png" | relative_url }} "MozReviewユーザー名")

#### APIキーの生成

自分がソースコードに対して行った変更をMercurialでreviewboard.mozilla.orgにpushするためには、BugzillaのAPIキーが必要になります。この後のMercurialのセットアップの際に必要になりますので、事前に生成しておきましょう。APIキーの作成はBugzillaアカウント設定画面の[API Keys](https://bugzilla.mozilla.org/userprefs.cgi?tab=apikey)ページで行うことができます。

![APIキーリスト]({{ "/images/blog/20180705_1.png" | relative_url }} "APIキーリスト")

#### Mercurialの追加セットアップ

MercurialでMozReviewと連携できるようにするためには、Mercurialに追加の設定が必要になります。この設定は[前回の記事]({% post_url 2018-07-03-index %})でも紹介した`./mach bootstrap`の中で対話的に行うことができます。ただし、前回はMozReviewについての説明は省略したため、それに関するセットアップは飛ばしてしまっているかもしれませんね。Mercurialを最初から設定し直したい場合は、既存の~/.hgrcを退避させて、`./mach mercurial-setup`で実行することができます。

```
$ cd /path/to/mozilla-central
$ mv ~/.hgrc ~/.hgrc.bak
$ ./mach mercurial-setup
```


後はコンソールに表示される指示に従って必要な項目（自分のフルネーム、メールアドレス、ニックネーム等）をセットアップして行けば良いです。Yes/Noで答えるものについては、基本的には全てYesで良いでしょう。

以下の項目については、一度MozReviewを設定してしまえは他の方法でパッチを投稿することは無いでしょうから、`1`で良いでしょう。

```
Commits to Mozilla projects are typically sent to MozReview. This is the
preferred code review tool at Mozilla.

Some still practice a legacy code review workflow that uploads patches
to Bugzilla.

1. MozReview only (preferred)
2. Both MozReview and Bugzilla
3. Bugzilla only

Which code review tools will you be submitting code to?  
```


以下の項目では、先ほどBugzillaで生成したAPIキーをコピペして入力します。

```
Bugzilla API Keys can only be obtained through the Bugzilla web interface.

Please perform the following steps:

  1) Open https://bugzilla.mozilla.org/userprefs.cgi?tab=apikey
  2) Generate a new API Key
  3) Copy the generated key and paste it here
```


`./mach mercurial-setup`が正常に終了したら、~/.hgrcを確認して必要な項目が適切に設定されていることを確認します。

```
[ui]
username = Your Name <your.name@example.com>

[extensions]
reviewboard = /path/to/home/.mozbuild/version-control-tools/hgext/reviewboard/client.py

[paths]
review = https://reviewboard-hg.mozilla.org/autoreview

[mozilla]
ircnick = nick

[bugzilla]
username = your.name@example.com
apikey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```


### レビューリクエストの作成

[前回の記事]({% post_url 2018-07-03-index %})で紹介したように、Firefoxで何かパッチを投稿したい場合は、全てBugzilla上の該当Bugを起点に作業を行います。何か新機能を追加したり、不具合修正を行いたい場合は、まず該当するBugが既に存在するかどうかを確認し、無い場合は新たに自分で新しいBugをfileします。

該当Bugでソースコードに対して何か変更を行って、いざMozReviewでパッチを投稿したいとなった場合、まずはMercurialで手元のリポジトリに変更をコミットします。

```
$ hg commit
```


このとき、コミットメッセージの形式に注意しましょう。具体的には以下のような形式にする必要があります。

```
Bug [Bug番号] - [Bugの概要（一行）]

以下、Bugの詳細についての記述...
```


Mercurialでリモートリポジトリにpushする際、上記のコミットメッセージのBug番号から自動的にBugzillaの該当Bugにパッチが投稿されます。

また、末尾に`r?ircnick`という形式でレビュアーを指定すると、push後に自動的に該当レビュアーにレビューリクエストを投げることもできます。このレビュアーの指定はpushした後にReview BoardのWeb UIから行うこともできますので、必ずしもコミットメッセージに含める必要はありません。

以下に、筆者が実際にパッチを投稿した際のコミットメッセージを示します。

```
Bug 1306529 - OmxDataDecoder: Fix a stall issue on shutting down r?jya

Because the shutdown closure awaits finishing itself by
TaskQueue::AwaitShutdownAndIdle(), the function blocks infinitely.

The code is wrongly introduced at the following commit:

  * https://bugzilla.mozilla.org/show_bug.cgi?id=1319987
    * https://hg.mozilla.org/mozilla-central/rev/b2171e3e8b69

This patch calls it on mTaskQueue intead of mOmxTaskQueue to
avoid the issue.
```


詳細な議論はBug番号から辿ることができるため、コミットメッセージには必ずしも詳細な記述は必要ないようです。有った方が好ましいとは思いますが、慣れていない場合には、まずはBug番号と一行サマリを適切に記載することに注力すると良いでしょう。

ローカルリポジトリへのコミットが完了したら、リモートリポジトリにpushします。

```
$ hg push review
```


pushが完了すると、以下のようにReview Board上のURLが表示されますので、後の操作はWeb UI上で行うことができます。

```
...
submitting 1 changesets for review

changeset:  424886:1e4bfbffe0de
summary:    Bug 1451816 - [Wayland] Avoid closing popup window on button-press on Weston r?stransky
review:     https://reviewboard.mozilla.org/r/254690 (draft)

review id:  bz://1451816/takuro
review url: https://reviewboard.mozilla.org/r/254688 (draft)
```


この状態では、レビューリクエストはまだ公開されておらず、レビュアーにも通知は届いていません。
最後に以下のようにレビューリクエストを公開するかどうかを尋ねられますので、

```
publish these review requests now (Yn)?  
```


ここで`y`を入力すると、レビューリクエストが公開され、レビュアーに通知されます。
ここではいったん`n`としておいて、Web UI上でレビュアーを指定した上でレビューリクエストを公開することも可能です。この場合は下記画面のReviewersでレビュアーを指定した後、`Publish`を押してレビューリクエストを公開します。

![Review Board]({{ "/images/blog/20180705_2.png" | relative_url }} "Review Board")

これでレビューリクエストは完了ですが、Bugzillaの該当Bugの方も確認して、Review Board側のレビューリクエストと正しく紐付けられていることを確認しておくと良いでしょう。

なお、先ほどコミットした内容を`hg export`で確認してみると、以下のように`MozReview-Commit-ID`という行が追加されていることに気が付きます。

```
# HG changeset patch
# User Your Name <your.name@example.com>
# Date 1530688340 -32400
#      Wed Jul 04 16:12:20 2018 +0900
# Node ID 1e4bfbffe0de2870b7539a8ed42b9b6d6721fc87
# Parent  987ea0d6a000b95cf93928b25a74a7fb1dfe37b2
Bug 1451816 - [Wayland] Avoid closing popup window on button-press on Weston r?stransky

MozReview-Commit-ID: 2khX59CotQK

...
```


これは後にレビュー結果を受けてパッチを修正する際に必要になります。

### パッチの修正

レビュアーによってパッチがレビューされ、Review Board上で修正箇所を指摘されたら、パッチを修正して再度Review Boardにpushすることになります。この際、同一のレビューリクエストに対する修正であることを指定するために、先ほどと同じ`MozReview-Commit-ID`をコミットメッセージに含めて`hg commit`し、`hg push review`します。

Mercurialでのパッチ管理方法は本記事のスコープ外のため割愛しますが、パッチ（コミット）が１つのみで、ローカルリポジトリに過去のバージョンが不要である場合、もっとも簡単な修正方法は`hg commit --amend`で前回のコミットをやり直す方法でしょう。この方法の場合、コミットメッセージは特に修正しなければ前回のままとなりますので、`MozReview-Commit-ID`も前回と同じものが使用されます。ローカルリポジトリの修正は上書きされてしまいますが、リモートリポジトリ上では過去のバージョンも管理され、その差分を確認することもできます。

![Review Board Diff]({{ "/images/blog/20180705_3.png" | relative_url }} "Review Board Diff")

修正をpushしたら、Review Board上でレビュアーのコメントに返信をします。この際も、最後に`Publish`ボタンを押すことを忘れないで下さい。なお、Review Board上の会話は自動的にBugzillaの該当Bugの方にも同じ内容が投稿されます。

### レビューが通ったら

レビュアーによってパッチが問題ないと判断された場合、`r?`あるいは`r-`となっていた部分が`r+`に変更されます。この状態になったら、[前回の記事]({% post_url 2018-07-03-index %})と同様に、Bugzilla上で「Keywords」欄に`checkin-needed`のキーワードを付加して、パッチをチェックインしてもらいます。

### まとめ

MozReviewでのFirefoxへのパッチ投稿方法について紹介しました。

なお、記事中で例として紹介した[Bug 1451816](https://bugzilla.mozilla.org/show_bug.cgi?id=1451816)はGNU/LinuxのWestonという環境でポップアップウィンドウのボタンを操作できないという問題です。本質的な修正は他のBugで行う必要があり、投稿しているパッチは必ずしも本体に入れるつもりの無いad-hocなパッチです。このようなパッチでも、アップストリームに報告しておけば同じ問題で困っている人の助けになるかもしれませんし、本質的な修正のヒントになる可能性もあります。自分のパッチに価値があるかないかは出してみないと分かりませんので、皆さんも臆することなくどんどんパッチを提出してみてはいかがでしょうか。

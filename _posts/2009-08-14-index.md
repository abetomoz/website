---
tags:
- cutter
title: PEから公開されている関数名を抜き出す
---
[ELFから公開されている関数名を抜き出す]({% post_url 2009-05-22-index %})、[Mach-Oから公開されている関数名を抜き出す]({% post_url 2009-06-25-index %})のPE（[Portable Executable](https://ja.wikipedia.org/wiki/Portable+Executable)）版です。PEはWindowsの.exeや.dllなどで利用されているファイルフォーマットです。
<!--more-->


artonさんがCodeZineで[DbgHelpを利用してDLLがエクスポートしている関数を列挙する](http://codezine.jp/article/detail/416)という同様の内容の記事を書いています。PEのフォーマットについても説明しているので、まず、この記事を読んでおくとよいでしょう。

ここでは、DbgHelpなどライブラリを一切使わずに自力でPEをパースし、関数名を抜き出します。そのため、[MinGW](https://ja.wikipedia.org/wiki/MinGW)でクロスコンパイルすることも簡単です。実際、CutterはMinGWを用いたクロスコンパイルに対応しています。

### 下準備

簡略化のためファイルの内容をすべてメモリに読み込んでから処理します。コツコツ資源を利用したい場合は少しづつ読み込みながら処理することになります。

ファイルの内容を読み込むために、便利な[GLibのg_file_get_contents()](http://library.gnome.org/devel/glib/stable/glib-File-Utilities.html#g-file-get-contents)を使いたいところですが、Windows環境ではGLibがインストールされていないことが多いので、ここでは自力で読み込むことにします。

{% raw %}
```c
char *content = NULL;
FILE *file;
char buffer[4096];
size_t bytes, total_bytes = 0;

file = fopen(argv[1], "rb");
if (!file) {
    perror("failed to fopen()");
    return -1;
}

while (!feof(file)) {
    char *original_content;

    bytes = fread(buffer, 1, sizeof(buffer), file);
    total_bytes += bytes;
    original_content = content;
    content = realloc(content, total_bytes);
    if (!content) {
        free(original_content);
        fclose(file);
        perror("failed to realloc()");
        return -1;
    }
    memcpy(content + total_bytes - bytes, buffer, bytes);
}
fclose(file);
```
{% endraw %}

これで、<var>content</var>の中にファイルの内容が格納されました。これを使って公開されている関数名を抜き出します。

PEのフォーマットに関する情報は`winnt.h`で定義されています。`winnt.h`は`windows.h`をincludeすると暗黙のうちにincludeされるので、`windows.h`だけincludeします。

{% raw %}
```c
#include <windows.h>
```
{% endraw %}

### PEかどうかを判断

まず、ファイルがPEかどうかを判断します。

PEであればNTヘッダに"PE\0\0"という署名が入っているので、これを確認します。"PE\0\0"という署名は`IMAGE_NT_SIGNATURE`というマクロとして定義されているので、これを利用します。

{% raw %}
```c
IMAGE_DOS_HEADER *dos_header;
IMAGE_NT_HEADERS *nt_headers;

/* ファイルの先頭はDOSヘッダ */
dos_header = (IMAGE_DOS_HEADER *)content;
/* NTヘッダを見つける */
nt_headers = (IMAGE_NT_HEADERS *)(content + dos_header->e_lfanew);
/* 署名が"PE\0\0"かどうか確認 */
if (nt_headers->Signature == IMAGE_NT_SIGNATURE) {
    /* PEファイル */
}
```
{% endraw %}

### DLLかどうかを判断

PEであることが確認できたら、DLLかどうかを確認します。

{% raw %}
```c
if (nt_headers->FileHeader.Characteristics & IMAGE_FILE_DLL) {
    /* DLL */
}
```
{% endraw %}

### 公開されているシンボルを探索し出力

公開されているシンボルはエクスポートデータセクションを見るとわかります。また、シンボルが関数かどうかは、実体がテキストセクションにあるかどうかで判断します。この方法が関数かどうかを判断する標準的な方法かはわかりませんが、実用上はこれで問題なさそうです。

よって、まず、エクスポートデータセクションヘッダとテキストセクションヘッダを見つけます。それぞれ、ヘッダの名前は以下のようになっているので、それを目印に見つけます。

<dl>






<dt>






エクスポートデータセクションヘッダ名






</dt>






<dd>


.edata


</dd>








<dt>






テキストセクションヘッダ名






</dt>






<dd>


.text


</dd>


</dl>

以下がソースコードです。

{% raw %}
```c
WORD i;
IMAGE_SECTION_HEADER *first_section;
IMAGE_SECTION_HEADER *edata_section;
IMAGE_SECTION_HEADER *text_section;

/* 最初のセクションヘッダ */
first_section = IMAGE_FIRST_SECTION(nt_headers);
for (i = 0; i < nt_headers->FileHeader.NumberOfSections; i++) {
    const char *section_name;

    section_name = (const char *)((first_section + i)->Name);
    /* 各セクションの名前を確認 */
    if (strcmp(".edata", section_name) == 0) {
        /* エクスポートデータセクションを発見 */
        edata_section = first_section + i;
    } else if (strcmp(".text", section_name) == 0) {
        /* テキストセクションを発見 */
        text_section = first_section + i;
    }
}
```
{% endraw %}

ヘッダが見つかったら、セクションの内容を見て、関数であるシンボル名を出力します。[^0]

{% raw %}
```c
IMAGE_EXPORT_DIRECTORY *export_directory;
const char *base_address;
ULONG *name_addresses;
ULONG *function_addresses;
DWORD min_text_section_address, max_text_section_address;

/* エクスポートデータセクションの内容 */
export_directory =
    (IMAGE_EXPORT_DIRECTORY *)(content +
                               edata_section->PointerToRawData);

/* エクスポートデータセクション内のデータがあるアドレスを解決するための
   基準になるアドレス */
base_address =
    content +
    edata_section->PointerToRawData -
    edata_section->VirtualAddress;
/* シンボル名があるアドレス */
name_addresses =
    (ULONG *)(base_address + export_directory->AddressOfNames);
/* シンボルの実体への相対的なアドレス(RVA)があるアドレス */
function_addresses =
    (ULONG *)(base_address + export_directory->AddressOfFunctions);
/* テキストセクションのデータの相対的なアドレスの下限 */
min_text_section_address = text_section->VirtualAddress;
/* テキストセクションのデータの相対的なアドレスの上限 */
max_text_section_address =
    min_text_section_address + text_section->SizeOfRawData;

/* シンボル名毎に関数かどうか判断 */
for (i = 0; i < export_directory->NumberOfNames; i++) {
    const char *name;
    DWORD function_address;

    /* シンボル名 */
    name = base_address + name_addresses[i];
    /* シンボルの実体の相対的なアドレス */
    function_address = function_addresses[i];
    if (min_text_section_address < function_address &&
        function_address < max_text_section_address) {
        /* シンボルの実体がテキストセクションにあるなら関数 */
        printf("found: %s\n", name);
    }
}
```
{% endraw %}

### 参考

  * 上記をまとめたソースコード: [list-pe-public-function-names.c](https://github.com/clear-code/list-symbols-in-shared-library/blob/master/list-pe-public-function-names.c)
  * CutterのPEから公開されている関数名を抜き出す部分のソースコード: [cut-pe-loader.c](http://cutter.svn.sourceforge.net/viewvc/cutter/cutter/trunk/cutter/cut-pe-loader.c?view=markup)

### まとめ

`winnt.h`を使って、DbgHelpなどに依存せずに、PEから公開されている関数名を抜き出す方法を紹介しました。

サンプルプログラムはDebian GNU/Linux上でMinGWを使ってクロスコンパイルし、[Wine](https://ja.wikipedia.org/wiki/Wine)で動作を確認しました。

今のところ、Cutterがサポートしている共有ライブラリのフォーマットはELF/Mach-O/PEです。このPE編で、公開されている関数名を抜き出す方法を紹介するシリーズは最後です。もし、今後Cutterが対応するフォーマットが増えれば、そのフォーマットから関数名を抜き出す方法を紹介するかもしれません。

[^0]: コメント中のRVAの説明はざっくりなので、詳しくは冒頭のartonさんの記事を参照してください。

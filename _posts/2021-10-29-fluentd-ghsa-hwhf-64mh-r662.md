---
tags:
- fluentd
title: Fluentd v1.14.2に関してGitHubセキュリティアドバイザリーを公開しました (GHSA-hwhf-64mh-r662)
author: kenhys
---

クリアコードでは[Fluentdのサポートサービス]({% link services/fluentd.md %})を提供しています。


今回は、Fluentd v1.14.2で脆弱性の修正を含むリリースを実施しました。
これにともない、GitHubセキュリティアドバイザリーを[公開](https://github.com/fluent/fluentd/security/advisories/GHSA-hwhf-64mh-r662)しています。
問題の発覚の経緯とどのように対応したのかについて説明します。

<!--more-->

## 脆弱性発覚の経緯

[GitHub Security Lab](https://securitylab.github.com/)から関係者に脆弱性の報告がなされたことにより、問題が発覚しました。

この問題は、Fluentdの`parser_apache2` プラグインがログをパースする際に、悪意のあるログを入力として与えるとサービス拒否を引き起こすというものです。
これは、該当プラグインが脆弱な正規表現を利用していることに起因する問題で、一般にReDoS(Regular expression Denial of Service)とよばれます。

GitHub Security Labからは、いくつかの再現条件を満たすことで再現性のある実証コードも提供されました。

なお、再現条件のうちの1つにユーザーエージェントの末尾にエスケープされていない`"`が含まれる必要性がありますが、
ユーザーエージェントを悪意のある文字列にしてアクセスしても、Apacheがエスケープしてログに出力します。
したがって、通常の利用でただちにサービス拒否に陥るリスクはそれほど高くありません。

ただし、`in_foward`などで外部から受け付けたログをApacheのログの形式に沿っているはずだからと`parser_apache2`で処理すると、今回の問題が起きる可能性はあります。

## GitHubでセキュリティーアドバイザリーをだすには

GitHubセキュリティーアドバイザリーを利用するには、[GitHub セキュリティアドバイザリについて](https://docs.github.com/ja/code-security/security-advisories/creating-a-security-advisory)のドキュメントの参照をおすすめします。

GitHubセキュリティアドバイザリーをだすまでのおおまかな流れは次のとおりです。

* リポジトリの管理者権限があるユーザーがセキュリティアドバイザリーを作成する
  * https://github.com/fluent/fluentd/security/advisories にNew draft security advisoryというボタンがあるのでそちらから作成できました
* 一時的なプライベートフォークを作成する
  * このプライベートフォークには関係者のみアクセスできます
  * ドラフト状態のセキュリティアドバイザリーから作成できます
* 作成したプライベートフォークでプルリクエストを作成する
* 関係者で議論しつつ、プルリクエストをマージする
* マージした修正をリリースする
* セキュリティアドバイザリーを公開する

なお、リリース直前までCHANGELOG等に不用意に脆弱性について記述しないように留意する必要がありました。

## GitHubセキュリティアドバイザリーを通じて得た知見

* GitHubでFluentdを使っているプロジェクトに脆弱性情報が自動的に通知されるので便利
* GitHubセキュリティアドバイザリー内で立てたプルリクエストは公開されない
  * 今回直前までプルリクエストは作らなかったが、修正内容について議論する場合はプルリクエストを作ってそこで議論した方が良い
* CVEのアサインのリクエストもGitHubセキュリティアドバイザリーから行える
  * 今回の問題に対してはCVE-2021-41186がアサインされた
* マージボタンはセキュリティアドバイザリ内に現れる
* マージボタン押すと直接公開リポジトリのmasterにマージされる

## 脆弱性関連情報について

* [CVE-2021-41186](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-41186)
* [ReDoS vulnerability in parser_apache2](https://github.com/fluent/fluentd/security/advisories/GHSA-hwhf-64mh-r662)
* [Fluentd v1.14.2 has been released](https://www.fluentd.org/blog/fluentd-v1.14.2-has-been-released)

## さいごに

まとめると、今回のGitHubセキュリティーアドバイザリーに関する内容は次のとおりです。

* Fluentd v0.14.14からv1.14.1の`parser_apache2`プラグインがこの脆弱性の影響を受けます
* Fluentdの`parser_apache2`を使っていないなら、この脆弱性の影響は受けません
* Fluentdの`parser_apache2`を使っていても、Apacheが生成したログであることが保証されるなら、この脆弱性の影響は受けません
* Fluentdをv1.14.2に更新するか、`parser_apache2`を脆弱性修正済みのものに置き換える必要があります

今回は、Fluentd v1.14.2のリリースにともない、GitHubのセキュリティーアドバイザリーを出したのでその解説をしました。

クリアコードの提供する[Fluentdのサポートサービス]({% link services/fluentd.md %})では、障害発生時の調査や回避策の提案、パッチの提供などを行います。
Fluentdに関するトラブルを抱えて困っている方は、ぜひこちらの[お問い合わせフォーム]({% link contact/index.md %})からご連絡ください。

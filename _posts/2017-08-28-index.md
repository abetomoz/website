---
tags: []
title: DebConf17のオススメのセッションを紹介します
---
### はじめに

今年のDebConf[^0]はカナダのモントリオールで今月の6日から12日にかけて[DebConf17として開催](https://debconf17.debconf.org/)されました。
<!--more-->


都合が合わなくて参加できなかった人でも、セッションのアーカイブが公開されているのであとから視聴することができます。（もちろん諸事情から非公開となっているものもあります。）

今回は、DebConf17の数あるセッションの中からオススメのアーカイブを1つ紹介します。
なお、先日開催された[東京エリアDebian勉強会(2017/08/19)](https://debianjp.connpass.com/event/62367/)のDebConf17参加報告で該当のセッションについて教えてもらいました。
次回の[東京エリアDebian勉強会は9/16(土)に新宿で開催](https://debianjp.connpass.com/event/65414/)とのことです。[^1]

### GnuPG 2.1 Explained for Everyone

[g新部](https://ja.wikipedia.org/wiki/新部裕)さんによるセッションです。

発表資料と当日の映像は以下からたどることができます。

  * [GnuPG 2.1 Explained for Everyoneセッション紹介ページ](https://debconf17.debconf.org/talks/32/) （[PDF](https://annex.debconf.org/debconf-share/debconf17/slides/32-gnupg21_explained_for_everyone.pdf)/[WebM](https://gensho.ftp.acc.umu.se/pub/debian-meetings/2017/debconf17/gnupg-2-1-explained-for-everyone.vp8.webm)）

<div class="video-640x480">
  <video controls preload="auto" width="640" height="480" data-setup="{}">
    <source src="https://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/gnupg-2-1-explained-for-everyone.vp8.webm" type="video/webm">
  </video>
  <div style="margin-bottom: 5px">
    <a href="https://debconf17.debconf.org/talks/32/" title="GnuPG 2.1 Explained for Everyone">GnuPG 2.1 Explained for Everyone</a>
  </div>
</div>


このセッションでは、GnuPGのDebianにおける採用状況や各種リリースバージョンの説明、そしてGnuPG 2.1の新機能とそのアーキテクチャ変更についてデモをまじえて解説していました。

見どころは、GnuPG 2.1でのアーキテクチャー変更に伴い、サブコンポーネントに分割された話を実際にロールプレイで紹介している部分です。ちょうどg新部さんはご家族でDebConf17に参加されたようで、発表の際もご家族でGnuPGのサブコンポーネントの振る舞いを分担してわかりやすくデモしていました。娘さんがgpg役を、息子さんはdirmngr役を務める、といった具合です。[^2]

### まとめ

今回は、DebConf17のアーカイブの中からオススメのセッションを1つ紹介しました。
他にもたくさんのセッションのビデオが公開されています。[^3] 興味があれば、あれこれみておくといいかもしれません。[^4]

[^0]: DebConfはDebian開発者が一同に集う開発者向けのイベント。

[^1]: POMERA DM200にdebianをインストールしてみた話が聞けるようですね。Debian関連で質問したいことなどあれば、ふらっと参加してみるのもよいでしょう。

[^2]: セッションのデモ中、息子のひろしくんはちょっと飽きていたようです。ご家族で何度も練習したのでしょうか。

[^3]: DebConf17のセッションのアーカイブは http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/ からまとめてダウンロードすることもできるようになっています。

[^4]: セッションのアーカイブはWebM VP8コーデックなのでFirefoxでも視聴に支障はありません。VP9にする話もあるようですが、Firefoxのサポート次第でしょうか。

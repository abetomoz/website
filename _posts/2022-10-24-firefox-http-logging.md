---
title: "FirefoxでUI上からHTTPログを採取する方法"
author: kenhys
tags:
  - mozilla
---

### はじめに

Firefoxには、UI上からHTTPログを記録する仕組みがあります。
今回はESR78以降で正式に導入された、比較的新しめのHTTPログを採取する方法を紹介します。
Firefoxでウェブサイトへのアクセスに問題が発生したときの解析に役立つはずです。

<!--more-->

### about:networkingとは

Firefoxのパネルメニューにはウェブ開発ツールなどの開発者向けの機能がありますが、それとは別にネットワーク通信に関する情報を取得するための特別なページとして、
`about:networking`があります。
Firefoxの延長サポート版であるESR [^esr] 68では実験的な扱いだったものが、ESR 78以降で正式な機能となりました。

[^esr]: Extended Support Release

![about:networkingにアクセスしたときの画面]({% link images/blog/firefox-http-logging/about-networking.png %})

その中の一つの機能として、HTTPログを採取することができるようになっています。
[問題の原因調査のためのログ収集のセオリー]({% post_url 2013-06-25-index %}) という2013年の記事では、環境変数を指定してログを採取するやりかたを紹介していました。
現在でも環境変数を利用したログの採取方法は有効 [^environment-variables] ですが、HTTPログの機能がまだ使えない古いバージョンのFirefoxでない限り、HTTPログを使って採取してもらうのがおすすめです。

[^environment-variables]: 現在のFirefoxでは、NSPR_LOG_MODULESやNSPR_LOG_FILEではなく、MOZ_LOGやMOZ_LOG_FILEを指定するのが推奨されています。

従来の環境変数をあらかじめする方法では、対象となるログモジュールやログファイルの保存先を切り替えるには、環境変数を設定しなおしてFirefoxを再起動する必要があります。
一方HTTPログを使ったログの採取では、事前の環境変数の定義は不要で、Firefoxを再起動することなく採取対象のログモジュールやログの保存先を切り替えることができるからです。

### HTTPログの採取の方法

HTTPログの採取の方法は次のとおりです。

1. Firefoxを起動する
2. URL欄に`about:networking`を入力してEnterキーを押下する
3. `about:networking`のページが表示されるので`HTTPログ`を選択する

![HTTPログを選択したときの画面]({% link images/blog/firefox-http-logging/select-http-logging.png %})

4. ログファイルの保存先は既定では、%TEMP%以下にlog.txt-xxx.moz_logとして保存します。他の場所に保存したければ、ログファイルを指定し「ログファイルを設定」をクリックします。
5. ログモジュールは既定では`timestamp,sync,nsHttp:5,cache2:5,nsSocketTransport:5,nsHostResolver:5`が設定されています。対象のログモジュールを変更したければ、ログモジュールを指定し、「ログモジュールを設定」をクリックします。
6. 記録開始をクリックする

![HTTPログの記録開始をクリックしたときの画面]({% link images/blog/firefox-http-logging/start-http-logging.png %})

7. 新規タブを開き、ログを採取したいサイトにアクセスする
8. `about:networking`のタブに戻り、ログの採取を終了するために「記録終了」をクリックする

![HTTPログの記録終了をクリックしたときの画面]({% link images/blog/firefox-http-logging/finish-http-logging.png %})

これで、指定した場所にHTTPログが保存されます。

ログモジュールの既定値 [^default-log-modules] では、時刻やHTTPアクセス、キャッシュやDNSの名前解決のログが記録されます。

ログ採取の詳細については、[HTTP Logging](https://firefox-source-docs.mozilla.org/networking/http/logging.html)を参照してください。

[^default-log-modules]: `timestamp,sync,nsHttp:5,cache2:5,nsSocketTransport:5,nsHostResolver:5`

### おわりに

今回は、FirefoxでUI上からHTTPログを採取する方法を紹介しました。
ウェブアクセスに支障があるケースなど、Firefox側で問題を切り分けるためにはHTTPログが必要となることがあります。
そのような場合には、上記手順でログを採取してみてください。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお問い合わせください。
  

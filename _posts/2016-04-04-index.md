---
tags: []
title: Rubyist Magazine（るびま）0053号に須藤へのインタビューが掲載
---
Rubyist Magazine（るびま）にはRubyist HotlinksというRubyistへインタビューする企画があります。0053号の[Rubyist Hotlinks 【第 36 回】](http://magazine.rubyist.net/?0053-Hotlinks)はクリアコードの社長である須藤へのインタビューです。クリアコードのビジネスについてやOSS Gateの話題もあるのでご覧ください。それらの話題にそんなに興味のない方でもぜひご覧ください。聞き手・野次馬のみなさんのおかげで読み物としておもしろいものになっています。
<!--more-->


須藤は2016年5月28日開催の[東京Ruby会議11](http://regional.rubykaigi.org/tokyo11/)で話すのですが、発表者の自己紹介欄をRubyist Hotlinksへのリンクにするだけでよいので大変便利です。

[るびま0053号](http://magazine.rubyist.net/?0053)はHotlinks以外にもおもしろい記事が揃っているので読んでみてください。過去の記事にもおもしろい記事はたくさんあります。バックナンバーもぜひお楽しみください。

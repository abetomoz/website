---
tags:
- mozilla
title: Firefox 29で削除されたセキュリティポリシー機能と、その代替手段
---
### 概要

Firefox 29において、古くから存在していたセキュリティポリシー設定のための機能（Configurable Security Policies、またはCapabilities、略してCAPS）の大部分が削除されました。
このエントリでは、CAPSで行っていた設定を、サイト別設定機能とクリアコードが開発・提供しているアドオンの組み合わせで代用する方法について解説します。
<!--more-->


### CAPSとは？

Firefoxには古くから、[Webサイトごとに詳細な権限設定を行うためのCAPSと呼ばれるセキュリティポリシー機能](http://kb.mozillazine.org/Security_Policies)が存在していました。
これは以下のような形で利用する物でした。

```javascript
// ポリシー一覧を定義
pref("capability.policy.policynames", "trusted");

// 既定のポリシーを定義
// JavaScriptの実行を禁止
pref("capability.policy.default.javascript.enabled", "noAccess");
// file:// で始まるURLの読み込みを禁止
pref("capability.policy.default.checkloaduri.enabled", "noAccess");
// クリップボードへの書き込み操作を禁止
pref("capability.policy.default.Clipboard.cutcopy", "noAccess");
// クリップボードの内容の読み取り操作を禁止
pref("capability.policy.default.Clipboard.paste", "noAccess");
// ウィンドウを開く事を禁止
pref("capability.policy.default.Window.open", "noAccess");
...

// 信頼済みサイト用のポリシーを定義
// ポリシーを適用するサイトの一覧を定義
pref("capability.policy.trusted.sites", "http://www.mozilla.org www.mozilla.com");
// 既定のポリシーで禁止した操作を許可
pref("capability.policy.trusted.javascript.enabled", "allAccess");
pref("capability.policy.trusted.checkloaduri.enabled", "allAccess");
pref("capability.policy.trusted.Clipboard.cutcopy", "allAccess");
pref("capability.policy.trusted.Clipboard.paste", "allAccess");
pref("capability.policy.trusted.Window.open", "allAccess");
...
```


この機能は設定のためのUIが存在しておらず、アドオン[NoScript](https://addons.mozilla.org/firefox/addon/noscript/)が内部的に利用する以外の場面では、一般にはほとんど使われていませんでした。
そのため、Firefox 29において大部分の機能が削除されました。
（参考：[Firefox 29の互換性情報における記述](https://www.fxsitecompat.com/ja/docs/2014/per-domain-configurable-security-policies-are-no-longer-available/)）
現在は以下の機能だけが残されています。

  * JavaScriptの実行の可否の設定：capability.policy.(ポリシー名).javascript.enabled

  * File URLの読み込みの可否の設定：capability.policy.(ポリシー名).checkloaduri.enabled
（Firefox 29では本体だけではこの機能は動作せず、アドオン[caps-fileuri](https://addons.mozilla.org/firefox/addon/caps-fileuri/)のインストールが必要です。）

これら以外の機能は、Firefox 29以降では全く利用できません。

### サイト別設定による代用

一方で、現在のFirefoxには「サイト別設定」と呼ばれる機能が備わっており、ポップアップブロック機能やパスワードの保存などについて、ホスト名単位で許可または禁止の権限を設定する事ができます。
これらのサイト別設定は、初期状態の権限は「不明」となっており、ポップアップブロック機能や、パスワードマネージャによるパスワードの保存機能などが発動した際に、ユーザにその後の操作が委ねられます。
その際に「今後は確認しない」などの選択を取ると、選択の結果がサイト別設定として保存され、以後は保存された設定が暗黙的に利用されるようになります。

実際に現在どのようなサイト別設定が保存されているかは、ロケーションバーに「about:permissions」と入力する事で確認できます。また、ポップアップブロック機能の許可サイト一覧やパスワードマネージャの例外サイト一覧も、本質的にはこの仕組みに基づいて提供されています。

以上のように、サイト別設定とCAPSは挙動もコンセプトも全く異なっています。
しかしながら、「プライバシー情報へのアクセスやユーザの利便性を損ない得る特別な操作は、ユーザが明示的に許可されない限りは禁止する」「特定のWebサイトについて、特別な操作を実行する許可を与える事ができる」という点に着目すると、用途によってはある程度はCAPSの代用として利用できそうです。

サイト別設定をCAPSの代用に使うには、以下のような事ができる必要があります。

  * サイト別設定を管理者が一括して管理できるようにする。

  * ユーザが任意にサイト別設定を保存できないようにする。

  * 保存済みのサイト別設定をユーザが削除できないようにする。

具体的な方法はいくつか考えられますが、ここではクリアコードが開発・提供しているアドオンを使った場合の手順をご紹介します。

### サイト別設定を管理者が一括して管理する

Firefoxには[Mission Control Desktop（MCD）](https://developer.mozilla.org/ja/docs/MCD/Getting_Started)と呼ばれる集中管理のための仕組みが備わっており、これを使うと、「プリファレンス」と呼ばれる仕組みに基づいて管理されている様々な設定を、管理者が一括して管理することができます。

しかしながら、サイト別設定はプリファレンスとは別の仕組みに基づいて管理されているため、MCDで集中管理する事ができません。
また、MCD以外の方法でサイト別設定を集中管理する事もできません。

クリアコードで開発している[Permissions Auto Registerer](https://addons.mozilla.org/firefox/addon/permissions-auto-registerer/)というアドオンを使用すると、サイト別設定をMCDで集中管理できるようになります。
具体的には、MCDで定義したサイト別設定を各クライアントに配布し、各クライアント上で当該アドオンによってその設定を実際のサイト別設定として反映する、ということができます。

Permission Auto Registerer用の設定をCAPS風に定義した例が、以下のコードです。
「sites」以外の設定の値は、「0」が「ユーザに尋ねる（初期状態）」、「1」が「許可」、「2」が「禁止」です。

```javascript
// 信頼済みサイトの一覧（スペース区切り）
pref("extensions.autopermission.policy.trusted.sites",
                                "www.example.com www.example.jp");
// Cookieの保存の可否
pref("extensions.autopermission.policy.trusted.cookie",         1);
// デスクトップ通知の可否
pref("extensions.autopermission.policy.trusted.desktop-notification", 1);
// DOMフルスクリーンの利用の可否
pref("extensions.autopermission.policy.trusted.fullscreen",     1);
// 位置情報APIへのアクセスの可否
pref("extensions.autopermission.policy.trusted.geo",            1);
// 画像の読み込みの可否
pref("extensions.autopermission.policy.trusted.image",          1);
// オフラインストレージの利用の可否
pref("extensions.autopermission.policy.trusted.indexedDB",      1);
// アドオンのインストールの可否
pref("extensions.autopermission.policy.trusted.install",        1);
// Webアプリケーションのオフラインキャッシュの利用の可否
pref("extensions.autopermission.policy.trusted.offline-app",    1);
// パスワードマネージャの利用の可否
pref("extensions.autopermission.policy.trusted.password",       1);
// マウスポインタを隠す事の可否
pref("extensions.autopermission.policy.trusted.pointerLock",    1);
// 広告などのポップアップウィンドウを開く事の可否
pref("extensions.autopermission.policy.trusted.popup",          1);
//// 以下はCAPS連携機能で、「sites」の設定に基づいて自動的に
//// 相当するCAPSの設定に変換されます。
// capability.policy.*.javascript.enabled に対応
pref("extensions.autopermission.policy.trusted.javascript",     1);
// capability.policy.*.checkloaduri.enabled に対応
pref("extensions.autopermission.policy.trusted.localfilelinks", 1);
```


見ての通り、CAPSの設定とほぼ同じ要領で設定できるようになっています。

### ユーザ自身によるサイト別設定の変更を禁止する

Permission Auto Registererによってサイト別設定を集中管理できるようになっただけでは、CAPSの代用としては不完全です。
というのも、このままでは、サイト別設定をユーザが任意に変更したり削除したり追加したりできてしまうからです。
サイト別設定をCAPSの代用として使うためには、管理者以外はサイト別設定を変更できないように制限する必要があります。

サイト別設定をユーザが変更できないようにする最も単純な方法は、スタイルシートを使ってUI要素を非表示にするというものです。
クリアコードで開発している[globalChrome.css](https://addons.mozilla.org/firefox/addon/globalchromecss/)というアドオンを使うと、ユーザースタイルシート相当のカスタマイズを管理者が行う事ができます。

サイト別設定に関する設定UIは各所に分散しています。
すべてのサイト別設定に関するUIを非表示にするスタイルシートの例は、以下の通りです。
（※見落としがある場合には、訂正しますのでご指摘下さい）

```css
/* Cookie */
:root#BrowserPreferences #cookiesBox,
:root#BrowserPreferences #acceptThirdPartyRow,
:root#BrowserPreferences #keepRow,
:root[windowtype="Browser:page-info"]
  #perm-cookie-row,
/* デスクトップ通知 */
:root[windowtype="Browser:page-info"]
  #perm-desktop-notification-row,
/* DOMフルスクリーン */
:root[windowtype="navigator:browser"]
  #full-screen-remember-decision,
:root[windowtype="Browser:page-info"]
  #perm-fullscreen-row,
/* 位置情報API */
:root[windowtype="navigator:browser"]
  #geolocation-notification menuitem[label="常に許可"],
:root[windowtype="navigator:browser"]
  #geolocation-notification menuitem[label="常に許可しない"],
:root[windowtype="Browser:page-info"]
  #perm-geo-row,
/* 画像の読み込み */
:root[windowtype="Browser:page-info"]
  #blockImage,
:root[windowtype="Browser:page-info"]
  #perm-image-row,
/* オフラインストレージ */
:root#BrowserPreferences #offlineNotify,
:root#BrowserPreferences #offlineNotifyExceptions,
:root[windowtype="Browser:page-info"]
  #perm-indexedDB-row,
/* アドオンのインストール許可 */
:root#BrowserPreferences #addonInstallBox,
:root[windowtype="navigator:browser"]
  #addon-install-blocked-notification menuitem[label="常に許可"],
:root[windowtype="navigator:browser"]
  #addon-install-blocked-notification menuitem[label="常に許可しない"],
:root[windowtype="Browser:page-info"]
  #perm-install-row,
/* パスワード保存 */
:root#BrowserPreferences #savePasswordsBox,
/* マウスポインタを隠す */
:root[windowtype="Browser:page-info"]
  #perm-pointerLock-row,
/* ポップアップブロック */
:root#BrowserPreferences #popupPolicyRow,
:root[windowtype="navigator:browser"]
  menuitem[observes="blockedPopupAllowSite"],
:root[windowtype="navigator:browser"]
  menuitem[observes="blockedPopupEditSettings"],
:root[windowtype="navigator:browser"]
  menuitem[observes="blockedPopupDontShowMessage"],
:root[windowtype="navigator:browser"]
  menuseparator[observes="blockedPopupsSeparator"],
:root[windowtype="Browser:page-info"]
  #perm-popup-row
{
  display: none !important;
  visibility: collapse !important;
}
```

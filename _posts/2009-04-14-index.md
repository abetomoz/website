---
tags:
- ruby
title: 'ActiveLdap: ldap_mapping'
---
これまで、ActiveLdapにはまとまった日本語の情報がありませんでしたが、[id:tashen](http://d.hatena.ne.jp/tashen/)さんが[ActiveLdapのチュートリアルを翻訳](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)してくれています。原文に最新の状況に追従していない部分があるため、いくつか古い情報もあるのですが、現在、最新の状況に追従するように作業が進んでいます。（最初の方はわりと最新の状況に追従しています。）
<!--more-->


せっかくなので、ldap_mappingのあたりを簡単に説明します。できるなら、この内容を本家のドキュメントにうまくマージしたいと思っています。

### ldap_mapping

ActiveRecordでは何もしなくてもカラムへアクセスすることができるのですが、ActiveLdapではldap_mappingでLDAPとRubyのオブジェクトを対応させる必要があります。ActiveRecordでは1つのテーブルが1つのクラスに対応し、各レコードがインスタンスに対応しますが、LDAPではそのような対応関係を自動的に判断することが難しいためです。どのようなエントリの集合を1つのクラスに対応させるかはアプリケーション毎に異なります。そのため、ActiveLdapでは明示的にユーザに指定してもらう方法をとっています。適切なデフォルト値がない場合はデフォルト値を提供しない方が混乱しないと思います。

ldap_mappingでは以下の3つの情報を使ってクラスに対応するエントリの集合を決めます。

  * objectClass
  * 検索対象のツリー
  * 検索範囲

### objectClass

オブジェクトクラスposixGroupに属しているエントリを対象とする場合は以下のようになります。

{% raw %}
```ruby
class Group < ActiveLdap::Base
  ldap_mapping :classes => ["posixGroup"], ...
end
```
{% endraw %}

これで、posixGroupに属するLDAPエントリそれぞれがGroupクラスのインスタンスに対応することになります。

もし、すべてのLDAPエントリを扱いたい場合はオブジェクトクラスtopを指定します。

{% raw %}
```ruby
class Entry < ActiveLdap::Base
  ldap_maaping :classes => ["top"], ...
end
```
{% endraw %}

topはすべてのエントリが属しているオブジェクトクラスなので、ldap_mappingにオブジェクトクラスtopを指定することで、すべてのエントリの集合とEntryクラスを対応させることができます。これは、LDAPツリーを表示するアプリケーションを作るときに便利です。

余談ですが、ActiveLdapにはActiveRecordのacts_as_tree相当の機能が標準で組み込まれています。LDAPはツリー構造なので標準で組み込まれていることは自然ですね。Entryクラスのインスタンスもchildrenやparentなどのメソッドが使えるため、簡単にツリー状のビューを作成することもできます。

### 検索対象のツリー

LDAPのエントリの集合は検索対象のツリーでも絞り込むことができます。以下のように、それぞれのツリーで扱いが異なる場合に役立ちます。

{% raw %}
```ruby
class Group < ActiveLdap::Base
  ldap_mapping :classes => ["posixGroup"],
               :prefix => "ou=Groups", ...
end

class AdministratorGroup < ActiveLdap::Base
  ldap_mapping :classes => ["posixGroup"],
               :prefix => "ou=AdministratorGroups", ...
end
```
{% endraw %}

Groupは通常ユーザ用のグループで、AdministratorGroupは管理者ユーザ用のグループです。多くの場合、役割毎にLDAPツリーをわけて管理していると思うので、それをRubyの世界でも利用するということです。

このようにクラスをわけることによって、メソッド内で管理者グループかどうかで処理を振り分けなくてもよくなります。

{% raw %}
```ruby
class Group < ActiveLdap::Base
  def users
    # 一般ユーザの配列を返す
  end
end

class AdministratorGroup < ActiveLdap::Base
  def users
    # 管理者ユーザの配列を返す
  end
end
```
{% endraw %}

例としてusersメソッドを出しましたが、実は、ActiveLdapにはActiveRecordのhas_manyのように、LDAPエントリ間の関連をRubyで簡単に扱えるようにする機能があり、usersのようなメソッドは自分で定義する必要はありません。別の機会にでも紹介したいと思います。

### 検索範囲

検索対象のツリーのうち、どのツリーを検索範囲にするのかを指定できます。

{% raw %}
```ruby
class Group < ActiveLdap::Base
  ldap_mapping :classes => ["posixGroup"],
               :prefix => "ou=Groups",
               :scope => :sub, ...
end
```
{% endraw %}

この例では、検索ツリー以下にあるサブツリー全体からエントリを検索します。もし、サブツリーの直下にだけ必要なエントリがある場合は:oneを指定することにより、よけいな検索を避けることができます。アプリケーションにあわせて適切な検索範囲を指定して下さい。

### まとめ

ActiveLdapでのLDAPのエントリとRubyのオブジェクトを関連付けるためのメソッドldap_mappingについて簡単に紹介しました。ここでは以下の3つについてだけ触れましたが、他にdn_attributeという重要なオプションがあります。

  * objectClass
  * 検索対象のツリー: LDAPでのbase
  * 検索範囲: LDAPでのscope

より詳しくは[ActiveLdapの日本語チュートリアル](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)を見てください。

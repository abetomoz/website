---
title: PC上でWeston + Wayland版Chromium + Fcitx5での日本語入力環境を構築する
author: daipom
tags:
- embedded
---

最近組み込み機器の日本語入力環境の改善に携わっている福田です。

主に[Fcitx5](https://fcitx-im.org/wiki/Fcitx_5)関連の拡張を行っています。最終的には組み込み機器上に実装することになりますが、大半の作業はPC上で実行環境を作って行います。

今回、[Weston](https://wayland.pages.freedesktop.org/weston/)上でWayland版のChromiumを動かし、Fcitx5で日本語入力ができる環境を構築したので、その手順を紹介します。

<!--more-->

## 環境

* Ubuntu 20.04.3 LTS Remix 64bit
* CPU: 11th Gen Intel® Core™ i7-11700
* memory: 31.1GiB
* window system: X Window System

## 注意点

* ユーザー名`daipom`は自分のユーザー名に置き換えてください。
* ビルドに用いるディレクトリに`~/build`を用いますが、自由に変更してください。
* シェルスクリプトを配置するディレクトリに`~/bin`を用いますが、自由に変更してください。
* ビルドした成果物は、標準パスではなくて、`/opt`配下にインストールして用います。
  * 後でバージョン毎に分けて管理できますし、開発でしか用いないものを標準パスに入れたくないからです。

## Weston

今回は、組み込み機器上の画面環境を再現するために、[Weston](https://wayland.pages.freedesktop.org/weston/)を用います。

[Weston](https://wayland.pages.freedesktop.org/weston/)は、[Wayland](https://wayland.freedesktop.org/)コンポジタのリファレンス実装です。Waylandにおいては、クライアントがサーバーに描画指示を行い、それを受けたサーバーが描画処理を行います。この通信プロトコルがWaylandであり、Waylandプロトコルを実装したサーバーをWaylandコンポジタと呼びます。このコンポジタの1種がWestonです。

### ビルド手順

今回は[Weston 5.0.0](https://gitlab.freedesktop.org/wayland/weston/-/tree/5.0)を用います。

Westonをビルドするために必要なものをインストールします。

```console
$ sudo apt build-dep weston
```

上操作で`ソースパッケージが見つかりません`と言われる場合は、以下を行ってください。デフォルトのUbuntuでは、`apt build-dep`によるインストールができません。

* `/etc/apt/sources.list`ファイルにおいて、コメントアウトされている`deb-src http://ooxx`をすべてコメント解除します。
* 一度`apt update`した後で、再度`apt build-dep`します。

```console
$ sudo sed -i -e 's/^# deb-src /deb-src /g' /etc/apt/sources.list
$ sudo apt update
$ sudo apt build-dep weston
```

[Westonのリポジトリ](https://gitlab.freedesktop.org/wayland/weston/)の`5.0`ブランチをcloneします。

```console
$ cd ~/build
$ git clone --branch=5.0 https://gitlab.freedesktop.org/wayland/weston.git
$ cd weston
```

`5.0.0`は以下のように`autogen.sh`と`make`を用いてビルドし[^uueston-build]、`/opt/weston`にインストールします。`weston-editor`を含めるために`enable-demo-clients-install`オプションを追加しています。

```console
$ ./autogen.sh --prefix=/opt/weston --enable-demo-clients-install
$ make -j$(nproc)
$ sudo make install
```

[^uueston-build]: 6以降はMesonを使ったビルドに変更されています。

### 実行

Westonの設定ファイル`~/.config/weston.ini`を次のように作成します。Westonのバージョンによってパス等が異なる可能性があるので、バージョンに応じた設定を行ってください。

```ini
[core]
idle-time=0
xwayland=true

[keyboard]
keymap_rules=evdev
keymap_layout=jp

[output]
name=X1
mode=1920x1200

[output]
name=wayland0
mode=1920x1200

[input-method]
path=/opt/weston/libexec/weston-keyboard

[launcher]
path=/opt/weston/bin/weston-terminal
icon=/opt/weston/share/weston/icon_terminal.png

[launcher]
path=/opt/weston/bin/weston-editor
icon=/opt/weston/share/weston/icon_editor.png
```

ここで設定した内容については、次の通りです。

* `[output]`の`name`について
  * 出力先に応じた値を設定する必要があります。
  * Ubuntuのデスクトップ環境上(ホスト側)にネストしてWestonを立ち上げるので、ホスト側のウィンドウシステムに対応する値を設定します。
    * `X11`ならば`X1`
    * `Wayland`ならば`wayland0`
  * ここでは両方設定しておくことで、どちらの場合でも動作するようにしています。
* `[input-method]`について
  * 入力に使うアプリケーションを設定します。
  * ここでは`weston-keyboard`を指定したため、Westonのソフトウェアキーボードを使って入力することができます。
  * ソフトウェアキーボードなしのシンプルな入力を行いたい場合は、`/opt/weston/libexec/weston-simple-im`を指定します。
  * 後でここをFcitx5に変更して、日本語入力を行えるようにします。
* `[launcher]`について
  * Weston内で立ち上げることのできるアプリケーションを設定します。
  * `weston-terminal`は、Weston内でコマンドを実行するために使います。
  * `weston-editor`は、簡易的なテキストエディタで、手軽にテキストの入力を試すのに便利です。
  * 後でここにChromiumも追加します。

次に、Westonを実行するためのシェルスクリプト`~/bin/run-weston.sh`を作成します。ファイルの権限は、`chmod +x`等で調整をします。

```sh
#!/bin/sh
export LD_LIBRARY_PATH="/opt/weston/lib:$LD_LIBRARY_PATH"
# export LD_LIBRARY_PATH="/opt/weston/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH" # Mesonベースの新しいWestonの場合はこちら
export PATH=/opt/weston/bin:$PATH
/opt/weston/bin/weston -c ~/.config/weston.ini "$@"
```

要点は以下です。

* 標準パスではなく、`/opt/weston`配下にインストールしたので、必要なパスを設定しています。
  * バージョン6以降のMesonベースのWestonでは、`LD_LIBRARY_PATH`に`/opt/weston/lib`ではなく、`/opt/weston/lib/x86_64-linux-gnu`を加える必要があります。
* 先ほど作成した設定ファイルを読み込んで動作するように設定しています。
* シェルスクリプトに渡した引数がそのままWestonに受け渡されるように設定しています。

すぐに`run-weston.sh`ファイルを実行できるように、`~/bin`にパスを通します。bashなら`~/.bashrc`などにリダイレクトしてください。

```console
$ echo 'export PATH="$PATH:$HOME/bin"' >> ~/.zshrc
$ source ~/.zshrc
```

以上でWestonを実行する準備ができました。`run-weston.sh`ファイルを実行して、Westonを立ち上げてみましょう。

```console
$ run-weston.sh
```

Westonが立ち上がったら、Westonの画面内で`weston-editor`を起動して、テキストにフォーカスしてみましょう。すると、`weston-keyboard`が表示され、テキストの入力ができます。表示されるソフトウェアキーボードを使わなくても、普通に入力することができます。

![weston]({% link /images/blog/setup-env-for-embedded-jp-software-keyboard-dev/weston.png %} "weston")

設定ファイルに誤りがある場合は、Weston実行時の出力で以下のようなメッセージが出ます。

```
Starting with no config file.
```

この場合は設定ファイルを修正し直してWestonを再実行してください。以下のようなメッセージが表示されれば大丈夫です。

```
Using config file '/home/daipom/.config/weston.ini'
```

### ネスト実行と単体実行について

上手順は、Ubuntuのデスクトップ環境上にネストしてWestonを立ち上げています（ホスト側の画面の中でWestonの画面が立ち上がっている）。別の方法として、ネストさせずにWeston単体で立ち上げることもできます。Ubuntuのログイン時に画面右下の歯車アイコンからWestonを選択することで、直接Westonが画面全体で立ち上がります。より実機に近い環境になりますが、機能が貧弱なため通常はネストで実行する方が便利です。

## Chromium

Weston上でChromiumを実行させてみます。

`weston-editor`でもテキストの入力は試せますが、最終的にはChromium上で正常にテキストを入力できることを確認します。

### ビルド手順

Chromiumのソースコードリポジトリ管理ツールである、[depot_tools](https://www.chromium.org/developers/how-tos/depottools)を準備します。次のようにcloneをしてパスを通します。bashなら`~/.bashrc`などにリダイレクトしてください。

```console
$ mkdir ~/build/chromium
$ cd ~/build/chromium
$ git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
$ echo 'export PATH="$PATH:$HOME/build/chromium/depot_tools"' >> ~/.zshrc
$ source ~/.zshrc
```

続いて次のようにソースコードを準備します。`fetch`コマンドはダウンロードに数10分程度かかります。

```console
$ fetch --nohooks chromium
$ cd src
$ ./build/install-build-deps.sh
$ gclient runhooks
```

`fetch`コマンドが途中でキャンセルされてしまった場合、再度行っても失敗してしまいます。その場合は、`gclient sync --nohooks`で再ダウンロードできます。

続いて次のようにビルド設定を行います。

```console
$ gn gen out/Wayland
$ gn args out/Wayland
```

エディタが起動するので、Wayland上で動作させるために以下のように設定を行います。

```sh
use_ozone=true
ozone_auto_platforms=false
ozone_platform_headless=true
ozone_platform_wayland=true
ozone_platform_x11=false
use_system_wayland_scanner=true
use_xkbcommon=true
use_system_libwayland=true
use_system_minigbm=true
use_system_libdrm=true
use_gtk=false
use_x11=false
```

最後に次のようにビルドを実行します。この環境で2時間弱かかりました。

```console
$ autoninja -C out/Wayland chrome
```

### Weston上でChromiumを実行

Chromiumを立ち上げるためのシェルスクリプト`~/bin/run-chrome.sh`を作成します。ファイルの権限は、`chmod +x`等で調整をします。

```sh
#!/bin/sh
~/build/chromium/src/out/Wayland/chrome --ozone-platform=wayland --enable-wayland-ime
```

Weston内でChromiumを立ち上げるために、Westonの設定`~/.config/weston.ini`に次のようなlauncherを追加します。

```ini
[launcher]
path=/home/daipom/bin/run-chrome.sh
icon=/opt/weston/share/weston/icon_flower.png
```

以上でWestonを立ち上げると、launcherが追加されており、そこからChromiumを立ち上げられるようになっています。

launcherを追加しなくても、`weston-terminal`からChromiumを立ち上げることも可能です。

## Fcitx5

[Fcitx5](https://fcitx-im.org/wiki/Fcitx_5)[^fcitx]は`Input Method`のフレームワークで、そのアドオンとなる`Input Method`エンジンを追加することで世界中の多様な言語の入力をサポートします。同様のものとしては他に[IBus](https://wiki.archlinux.jp/index.php/IBus)などがあります。FcitxはCJKV[^cjkv]ユーザーに好まれる傾向があるようです。

今回はこのFcitx5と、日本語入力用のエンジンとして[fcitx5-anthy](https://github.com/fcitx/fcitx5-anthy)を使います。

[^fcitx]: Fcitxは、`ファイテクス`のように読みます。
[^cjkv]: Chinese, Japanese, Korean, Vietnamese

### ビルド手順: Fcitx5

Fcitx5をビルドするのに必要なものをインストールします。

```console
$ sudo apt build-dep fcitx5
```

[Fcitx5のGitリポジトリ](https://github.com/fcitx/fcitx5)をcloneしてビルドし、`/opt/fictx5`にインストールします。

```console
$ cd ~/build
$ git clone git@github.com:fcitx/fcitx5.git
$ cd fcitx5
$ cmake -B build -DCMAKE_INSTALL_PREFIX=/opt/fcitx5
$ make -C build -j$(nproc)
$ sudo make -C build install
```

成功すれば、以上でFcitx5本体のビルドは完了です。次章のfcitx5-anthyのビルド手順へ進んでください。

2021-11-10時点で、私の環境（`Ubuntu 20.04`）においては、`xcb-imdkit`の問題でmakeが以下のように失敗しました。

```
.../fcitx5/src/frontend/xim/xim.cpp:114:13: error: ‘xcb_im_set_log_handler’ was not declared in this scope
.../fcitx5/src/frontend/xim/xim.cpp:116:9: error: ‘xcb_im_set_use_sync_mode’ was not declared in this scope
```

パッケージとしてインストールできる`xcb-imdkit`が古いことが原因のようです。この場合は次のようにインストールされている`xcb-imdkit`パッケージを削除し、代わりにビルドしたものを用いることで解決します。

インストールされている`xcb-imdkit`関連のパッケージ名を探します。

```console
$ apt list --installed | grep xcb-imdkit
```

見つかったものを削除します。私の場合は以下でした。

```console
$ sudo apt purge libxcb-imdkit-dev:amd64
$ sudo apt purge libxcb-imdkit0:amd64
```

[xcb-imdkitのGitリポジトリ](https://github.com/fcitx/xcb-imdkit)をcloneしてビルドし、`/opt/fictx5`にインストールします。Fcitx5と同じ場所にインストールすることで、環境変数の設定を行わなくて済みます。

```console
$ cd ~/build
$ git clone git@github.com:fcitx/xcb-imdkit.git
$ cmake -B build -DCMAKE_INSTALL_PREFIX=/opt/fcitx5
$ make -C build -j$(nproc)
$ sudo make -C build install
```

以上でFcitx5のmakeが成功するはずです。

### ビルド手順: fcitx5-anthy

まず、Ubuntuにanthy-unicodeがないようなので、これを入れます。

[anthy-unicodeのGitリポジトリ](https://github.com/fujiwarat/anthy-unicode)をcloneしてビルドし、Fcitx5と同じ場所`/opt/fcitx5`にインストールします。

```console
$ sudo apt build-dep anthy
$ cd ~/build
$ git clone git@github.com:fujiwarat/anthy-unicode.git
$ cd anthy-unicode
$ ./autogen.sh --prefix=/opt/fcitx5
$ make -j$(nproc)
$ sudo make install
```

続いて、[fcitx5-anthyのGitリポジトリ](https://github.com/fcitx/fcitx5-anthy)をcloneしてビルドし、これもFcitx5と同じ場所`/opt/fcitx5`にインストールします。`/opt/fcitx5`にインストールしたFcitx5とanthy-unicodeがビルドに必要なので、`-DCMAKE_PREFIX_PATH=/opt/fcitx5`オプションを加えています。

```console
$ cd ~/build
$ git clone git@github.com:fcitx/fcitx5-anthy.git
$ cd fcitx5-anthy
$ cmake -B build -DCMAKE_PREFIX_PATH=/opt/fcitx5 -DCMAKE_INSTALL_PREFIX=/opt/fcitx5
$ make -C build -j$(nproc)
$ sudo make -C build install
```

以上ではFcitx5と同じ場所にインストールすることで、環境変数の設定を行わなくて済んでいます。もしFcitx5とfcitx5-anthyのバージョンの組み合わせを複数試したい場合などは、別々の場所にインストールするのが良いでしょう。

最後に、Fcitx5の設定ファイル`~/.config/fcitx5/profile`を次のように編集して、fcitx5-anthyを用いるように設定します。

* `[Groups/0]`の`DefaultIM`を`anthy`に設定
* `[Groups/0/Items/0]`の`Name`を`anthy`に設定

```ini
[Groups/0]
# Group Name
Name=既定
# Layout
Default Layout=us
# Default Input Method
DefaultIM=anthy

[Groups/0/Items/0]
# Name
Name=anthy
# Layout
Layout=

[GroupOrder]
0=既定
```

### Weston上で日本語入力

Westonの`Input Method`としてFcitx5を実行するためのシェルスクリプトを作成します。

まず、Westonが用いる`DISPLAY`環境変数の値を調べます。Weston内で`weston-terminal`を立ち上げ、以下のコマンドで値を確認します。

```console
$ env | grep DISPLAY
```

通常は、以下のような値になっているはずです。

* ホストのウィンドウシステムが`X11`ならば`:1`
* ホストのウィンドウシステムが`Wayland`ならば`:0`

続いて、シェルスクリプト`~/bin/fcitx-weston.sh`を次のように作成します。ファイルの権限は、`chmod +x`等で調整をします。

```sh
#!/bin/sh
export DISPLAY=:1 # ここに調べた値を入れます
export LD_LIBRARY_PATH="/opt/fcitx5/lib:$LD_LIBRARY_PATH"
/opt/fcitx5/bin/fcitx5 --verbose "*=5"
```

要点は以下です。

* `DISPLAY`環境変数には最初に調べたWestonが用いる値を入れます[^display]。
* ビルドしたFcitx5はパスに通っていないので、`LD_LIBRARY_PATH`に加えています。
* ログはWestonと一緒に標準出力・標準エラー出力に出すことにしています。

続いて、Westonにおいて`Input Method`にFcitx5を用いるため、`~/.config/weston.ini`を編集します。`[input-method]`セクションの`path`に、先ほど作成したシェルスクリプトのパスを設定します。

```ini
...

[input-method]
path=/home/daipom/bin/fcitx-weston.sh

...
```

以上で、Weston上の`weston-editor`やChromiumにおいてFcitx5による日本語のテキスト入力が可能になります。

![fcitx5]({% link /images/blog/setup-env-for-embedded-jp-software-keyboard-dev/fcitx5.png %} "fcitx5")

このときWestonを起動した際にFcitx5が立ち上がることによって、ホスト側で日本語が入力できなくなることがあります。Westonを停止するとFcitx5も停止するので、その後にホスト側の`Input Method`を立ち上げ直すことで直ります。ホストがIBusを用いている場合は、Westonを停止し次のコマンドを実行することで直ります。

```console
$ ibus -daemon -drx
```

[^display]: `DISPLAY`環境変数については、Weston自体は空いている番号を自動で判定して動作します。しかし`[input-method]`セクションで設定されているプログラムが起動する時点では、まだ`DISPLAY`環境変数がセットされていないようです。そのためWeston上でFcitx5が正常に起動するためには、Westonが用いる`DISPLAY`環境変数の値をFcitx5に教えてあげる必要があります。

## Fcitx5をgdbでデバッグ

Fcitx5関連の拡張を行うに当たり、Fcitx5とfcitx5-anthyをデバッグする必要があります。gdbコマンドでデバッグを行う1例を紹介します。

まず、Fcitx5とfcitx5-anthyについて、デバッグ情報を付与してビルドし直します。念の為にビルドに関するデータをクリアした上で、`-DCMAKE_BUILD_TYPE=Debug`オプションをcmakeに追加してビルドし直します。

```console
$ rm -Rf ~/build/fcitx5/build/*
$ rm -Rf ~/build/fcitx5-anthy/build/*

$ cd ~/build/fcitx5
$ cmake -B build -DCMAKE_INSTALL_PREFIX=/opt/fcitx5 -DCMAKE_BUILD_TYPE=debug
$ make -C build -j$(nproc)
$ sudo make -C build install

$ cd ~/build/fcitx5-anthy
$ cmake \
  -B build -DCMAKE_PREFIX_PATH=/opt/fcitx5 \
  -DCMAKE_INSTALL_PREFIX=/opt/fcitx5 \
  -DCMAKE_BUILD_TYPE=debug
$ make -C build -j$(nproc)
$ sudo make -C build install
```

Westonを起動し、Fcitx5プロセスにgdbでattachします[^attach]。

```console
$ gdb
(gdb) attach {Fcitx5プロセスのpid}
```

`Could not attach to process. ` のようなメッセージが出てattachできない場合、Ubuntuでは以下の手順が必要です。

* `/proc/sys/kernel/yama/ptrace_scope`ファイルの内容を`1`から`0`に変更してください。
* 上方法だけでは再起動等でまた`1`に戻ってしまいます。永続的にattachできるようにするには以下も行ってください。
  * `/etc/sysctl.d/10-ptrace.conf`ファイルにおいて、`kernel.yama.ptrace_scope`の値を`1`から`0`に変更。

attachに成功したら、ブレークポイントを貼るなどしてデバッグすることができます。以下の点に注意してください。

* attachした時点でFcitx5の動作は一時停止しています。
  * attachすると日本語が入力できなくなりますが、それはattachにより一時停止しているだけです。
  * ブレークポイントを設定するなどして、`c`(`continue`)で動作を再開させてください。
* デバッグ後は、Westonを停止する前に`detach`をしてください。
  * detachせずにWestonを停止すると、Fcitx5のプロセスが停止できずに残ってしまい、ホスト側の入力がおかしくなることがあります。

[^attach]: Fcitx5プロセスのpidは`ps | grep /opt/fcitx5/bin/fcitx5`などで調べます。

## まとめ

日本語入力環境の改善を行うための環境として、Weston上でFcitx5による日本語入力をWayland版のChromiumに行うことのできる環境の作り方を紹介しました。

私は元々WaylandやFcitx5について全然知らなかったのですが、この環境を作りFcitx5の動作を検証しているうちに、少しずつ改善を行えるようになってきました。

例えば、fcitx5-anthyの修正を行い、無事マージされました（ https://github.com/fcitx/fcitx5-anthy/pull/2 ）。これは、変換候補をクリックで選択した際に、うまく画面が更新されない問題を修正するものです。なぜEnterキー等での選択では問題が発生しないのか、他のエンジン[^chinese-engine]ではどうしているのか、を調べたところ、fcitx5-anthyのクリック時の処理にFcitx5の画面更新を呼ぶ処理が抜けていることに気づき、直すことができました。

以上の内容が、組み込み機器などの日本語入力環境を改善したいと思われている方に、少しでも参考になれば幸いです。

[^chinese-engine]: [fcitx5-chinese-addons](https://github.com/fcitx/fcitx5-chinese-addons)のコードを参考にしました。

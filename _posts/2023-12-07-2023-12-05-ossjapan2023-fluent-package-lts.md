---
title: '#OSSummit Open Source Summit Japan 2023にてFluent Package LTSについて発表しました'
author: kenhys
tags:
- fluentd
---

2023年12月5日、Open Source Summit JAPAN 2023において、「New Chapter of Fluentd, Rebranding and New Release Cycle (LTS)」と題して
Fluentdの長期サポートを提供するためのパッケージに関する発表を行いました。
当日の発表内容を改めて紹介します。

<!--more-->

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/daipom/open-source-summit-japan-2023-fluent-package/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/daipom/open-source-summit-japan-2023-fluent-package/" title="New Chapter of Fluentd, Rebranding and New Release Cycle (LTS)">New Chapter of Fluentd, Rebranding and New Release Cycle (LTS)</a>
  </div>
</div>

* [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/daipom/open-source-summit-japan-2023/)
* [リポジトリー](https://gitlab.com/clear-code/fluentd-support-common/open-source-summit-japan-2023)


> Open Source Summit Japanは、日本で開催される大規模なカンファレンスで、オープンソース エコシステムが一堂に会します。技術者やオープンソース リーダー企業が、コラボレーションと情報共有のために、そして最新のオープンソース技術を学ぶために、あるいは革新的なオープン ソリューションを使った競争力の付け方を見つけるために集結します。
> Open Source Summitは、今日のオープンソースに影響を与える最も重要な技術、トピック、および問題をカバーするイベントの集まりで構成される、カンファレンス アンブレラです。

公式サイトにあるように、Open Source Summitはさまざまなイベントを併催しています。
FluentdはCloud Native Computing Foundation (CNCF)により認定されたプロジェクトの一つでもあるので、そのうちの1つであるCloudOpenが内容として適切であろうということでそちらで発表しました。

発表内容は後日アーカイブとして公開されるはずなので、詳細はそちらをみていただくとよいのですが、おおむね次のトピックについて話しました。

* Fluentdとは
* Fluentdの歴史について
* Fluent Package LTSの変更点
* td-agent v4からのアップグレードで留意すべきこと
* 主要な変更点について

従来、Fluentdのパッケージでインストールできるものとして、もともとTreasure Data社によって開発されてきたtd-agent [^third-party]がありました。

[^third-party]: ほかにもサードパーティーによるディストリビューションもありますが、独自に開発されている商用製品であるためここでは言及していません。

しかし、次のようなFluentdのパッケージをとりまく状況の変化があったことから、名称とその実態があっていないという状態になっていました。

* 次第にFluentdやそのパッケージの開発の主体がよりコミュニティーベースへと変化した
* Treasure Data CDP専用というよりかはより汎用的なデファクトパッケージとして使われている
* サポートポリシーが明確になっておらず事前の計画的なアップグレードが困難であった
* 安定した運用のためにより長期サポートのニーズがある

そこで、メジャーバージョンアップにあわせて、コミュニティーでの議論[^discussions]をもとにパッケージ名を変更[^rebrand]しました。

[^discussions]: https://github.com/fluent/fluentd/discussions/3860

[^rebrand]: ボツになったパッケージ名にはfluentd-ltsやfluentd-rolling、fluentd-bundleなんてものもありました。

従来のtd-agentバージョン4からのアップグレードについては、すでに[Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)という記事が公開されていますが、注意が必要なところについても説明しています。

今回の発表を通じて、依然として古いバージョンのtd-agentを使い続けているケースが多いことがあらためて浮き彫りになりました。
fluent-package LTSの提供開始により、td-agent v4のユーザーが移行しやすい状況になっていることが広く認知されることを期待しています。

<div class="callout secondary">
fluent-packageの現時点の最新版はv5.0.2ですが、v5.0.1をリリースしたときに<a href="https://www.youtube.com/watch?v=CrsTNHqWHTc">Fluent Package LTS v5.0 (Fluentd長期サポートパッケージリリース自慢会)</a>の動画を公開しました。こちらは日本語での解説となっています。あわせて参考にしてみてください。
</div>

なお、事前にアナウンス [^eol]しているとおり、td-agent v4のサポートは2023年12月末にて終了します。移行はお早めに。

[^eol]: [Drop schedule announcement about EOL of Treasure Agent (td-agent) 4](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)という記事を注意喚起のため8月に公開しています。

移行に関して、できるだけユーザーが困らない形にしてあるはずですが、もし何かバグを発見されたらGitHubの[issueに報告](https://github.com/fluent/fluent-package-builder/issues/new)してもらえると助かります。

もし移行もしくはその他のご相談がありましたら、サポート/コンサルタントを[Fluentdのサポートサービス]({% link services/fluentd-service.md %})として提供しているので、[お問い合わせフォーム]({% link contact/index.md %})よりお気楽にお問い合わせください。




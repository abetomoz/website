---
tags:
- ruby
title: '地獄のジェネレータ: Ruby Advent Calendar jp: 2010の24日目'
---
注: この記事にライセンスは設定されていません。
<!--more-->


これは[Ruby Advent Calendar jp: 2010](http://atnd.org/events/10430)の参加エントリです。前日は[ysakakiさん](http://ysakaki.com/)でした。

今年のAdvent Calender界隈では[惚れさせを集めたり](http://june29.jp/2010/12/17/glamorous-rubysappororecipe/)、[惚れさせを作ったり](http://blog.livedoor.jp/sasata299/archives/51518381.html)、[忙しい人でも惚れさせを見れるようにしたり](http://d.hatena.ne.jp/kei-s/20101223/1293084650)しているので、もう一歩先を行くことにしました。

### 地獄の名札ジェネレータ

[名札には名前を大きく書きましょうジェネレータ]({% post_url 2010-08-18-index %})に「[地獄モード](http://jigoku.nameplate.clear-code.com/)」が追加されました。

[![地獄の名札ジェネレータ]({{ "/images/blog/20101224_0.png" | relative_url }} "地獄の名札ジェネレータ")](http://jigoku.nameplate.clear-code.com/)

これを使うと[札幌Ruby会議03のプロの無職](http://d.hatena.ne.jp/m_seki/20101205#1291480951)を楽しむことができます。

![戦力外（フォントはIPA Pゴシック）]({{ "/images/blog/20101224_1.png" | relative_url }} "戦力外（フォントはIPA Pゴシック）")

[![ちゃんとした会社員]({{ "/images/blog/20101224_2.png" | relative_url }} "ちゃんとした会社員")](http://www.druby.org/Drop.pdf)

### 地獄のコマンドラインジェネレータ

地獄の名札ジェネレータはTwitterのプロフィール画像や説明文から惚れさせ名札を生成します。フォームに自分のTwitter IDを（カチャカチャカチャ…）と入力して（ッターン！）と「生成！」できるのでお手軽ですが、もっと惚れさせるために試行錯誤したい場合は少し不便ですね。

![エンターキー]({{ "/images/blog/20101224_3.png" | relative_url }} "エンターキー")

ということで、[ささたつさんが作ったmisawa gem](https://github.com/sasata299/misawa)を[画像生成に対応させ](https://github.com/kou/misawa)ました。

{% raw %}
```
% sudo gem install gtk2
% wget https://github.com/kou/misawa/raw/master/lib/misawa.rb
% wget -O misawa_background.jpg http://a2.twimg.com/profile_images/461389564/aaa.jpg
% ruby -r misawa -e 'misawa("俺ってそんなに\n唐揚げとたいやき食べてる\nイメージあるー？\n\nそれどこ情報？どこ情報よー？")'
% display misawa.png
```
{% endraw %}

![どこ情報？]({{ "/images/blog/20101224_4.png" | relative_url }} "どこ情報？")

### 縦書き

名言はPangoとcairoで描画しています。この2つのフリーソフトウェアを組み合わせることにより日本語の縦書きも描画できます。「ー」など単純に回転させるだけだと問題が起こる文字も、フォントが対応していれば[^0]きちんと描画できます。

ポイントは[PangoGravity](http://library.gnome.org/devel/pango/stable/pango-Vertical-Text.html#PangoGravity)と[cairo_rotate()](http://cairographics.org/manual/cairo-Transformations.html#cairo-rotate)です。

Pangoにはpango-viewというPangoのオプションを簡単に試せるツールがついています。これを使ってオプションの効果を確認します。

まず、そのまま描画した場合です。横書きになっていますね。

{% raw %}
```
% pango-view --output normal.png --font "MotoyaLMaru 48" --text "つれーわー"
```
{% endraw %}

![実質（そのまま）]({{ "/images/blog/20101224_5.png" | relative_url }} "実質（そのまま）")

次に、PangoGravityにPANGO_GRAVITY_EASTを指定した場合です。横に転がっていますが縦書きになっています。

{% raw %}
```
% pango-view --output east.png --font "MotoyaLMaru 48" --text "つれーわー" --gravity east
```
{% endraw %}

![実質（PANGO_GRAVITY_EAST）]({{ "/images/blog/20101224_6.png" | relative_url }} "実質（PANGO_GRAVITY_EAST）")

ここまでくればあとは実質回転させるだけだからつれーわー。最後に、-90度回転させれば縦書きが完成です。

{% raw %}
```
% pango-view --output east-rotate.png --font "MotoyaLMaru 48" --text "つれーわー" --gravity east --rotate -90
```
{% endraw %}

![実質（PANGO_GRAVITY_EASTと-90度回転）]({{ "/images/blog/20101224_7.png" | relative_url }} "実質（PANGO_GRAVITY_EASTと-90度回転）")

この縦書き機能はPango 1.16からサポートされています。Pango 1.16は2007年にリリースされているので、4年くらい前に見て、一番先に飽きた人もいるのではないでしょうか。

もちろん、Ruby/PangoでもPangoGravityを使うことができます。具体的な使い方は[名札ジェネレータ](https://github.com/kou/nafuda_generator/)か[misawa gem](https://github.com/kou/misawa/)を見てください。misawa gemの方がシンプルでわかりやすいでしょう。

名言を描画するところでは、文字を縦書きで描画するだけではなく、縦書きの文字に白い影もつけています。そのあたりのやり方もソースを見ればわかるでしょう。

### まとめ

惚れさせるために必要なRubyで縦書きを描画する方法を紹介しました。みなさんもRuby/Pangoとrcairoを使って縦書きなクリスマスカードや年賀状をRubyで生成してみてはいかがでしょうか。

明日の[Ruby Advent Calendar jp: 2010](http://atnd.org/events/10430)は[tmaedaさん](http://tmaeda.s45.xrea.com/td/)です。最後なのでとてもすごいのがくるはずですよ！

[^0]: たぶん。フォントによって問題が起きたり起きなかったりするから。

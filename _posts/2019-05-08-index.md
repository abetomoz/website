---
tags:
  - apache-arrow
  - python
  - presentation
title: 'SciPy Japan Conference 2019 - Apache Arrow #scipyjapan'
---
ゴールデンウィークの前のことになりますが、[SciPy Japan Conference 2019](https://www.scipyjapan2019.scipy.org/)で[Apache Arrow](https://arrow.apache.org/)の紹介をした須藤です。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2019/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2019/" title="Apache Arrow - A cross-language development platform for in-memory data">Apache Arrow - A cross-language development platform for in-memory data</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2019/)

  * [スライド（SlideShare）](https://slideshare.net/kou/scipy-japan-2019-apache-arrow)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-scipy-japan-2019)

### 経緯

まず、ふだんほとんどPythonを書いていない私がどうしてSciPy Japan Conference 2019で話すことになったのか、その経緯を説明します。

もともと、[Wes McKinneyさん](https://wesmckinney.com/)にオファーがありました。彼はPythonでよく使われているpandasの作者でもあり、Apache Arrowの主要開発者でもあります。SciPy Japan Conference 2019でApache Arrowの紹介をする人としてはうってつけです。しかし、残念ながらSciPy Japan Conference 2019の開催日に日本に来ることができませんでした。そこで、彼が私を紹介しました。私もApache Arrowの主要開発者の1人（2019-04-23時点では[Apache Arrowへのコミット数は2位](https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2019/?page=9)）で、私は東京に住んでいるからです。

ということで、Wesさんからの紹介で私がApache Arrowの紹介をすることになりました。

### 内容

Pythonのカンファレンスなので、Apache Arrowの一般的な話というより、Pythonユーザーにはどううれしくなりそうかという観点で紹介したつもりです。具体的には近い将来うれしくなりそうな点として次の2点を紹介しました。

  * pikcleの代わりに使って高速化できる

  * データフレームライブラリーが高速化したり大量のデータを扱えるようになる

すでにこれらの点を実現しているプロダクトがあります。

たとえば、Sparkはpickleの代わりにApache Arrowを使うことで100倍以上高速化しています。

参考：[Speeding up PySpark with Apache Arrow](https://arrow.apache.org/blog/2017/07/26/spark-arrow/)

たとえば、[Vaex](https://github.com/vaexio/vaex/)というデータフレームライブラリーはApache Arrowを（も）使うことでpandasよりも高速に文字列を処理できるようになっています。

参考：[Vaex: A DataFrame with super strings](https://towardsdatascience.com/vaex-a-dataframe-with-super-strings-789b92e8d861)

また、どうして高速になるか、大量のデータを扱えるようになるかの理由も説明しました。詳細はスライドや[Apache Arrowの最新情報（2018年9月版）]({% post_url 2018-09-05-index %})や[Apache Arrow東京ミートアップ2018 - Apache Arrow]({% post_url 2018-12-10-index %})を参照してください。

### イベントの内容

SciPy Japan Conferenceは今回が初めての開催ということで参加者は100人いかないくらいでした。PyCon JPと比べると小さな規模です。（PyCon JP 2018は[1000人以上の参加者](https://pyconjp.blogspot.com/2018/09/pyconjp-20182019.html)です。）初めての開催ということであまり知られていなかったこととサイエンスに特化した内容ということが影響しているのかと思います。

私は初めてPythonのイベントに参加しましたが、とても国際カンファレンスですごいなぁと思いました。私は海外で開催されている国際カンファレンスには参加したことがなく、参加したことがある国際カンファレンスはRubyKaigiだけなのですが、海外で開催されている国際カンファレンスはこんな感じなのかなぁと思いました。

SciPy Japan Conference 2019の運営をしていた方々から話を聞いたところ、SciPy Japan Conference 2019は本家のSciPy Conferenceを運営している人たちが運営しているということでした。本家のSciPy Conferenceについても教えてもらえました。「今回は機械学習の話題が多かったけど、本家のSciPy Conferenceは機械学習だけでなくサイエンス全般の話題を扱っているんだよ。地球のこととか。SciPy Japan Conferenceもサイエンス全般の話題を扱えるようになるといいな。」みたいな話を聞いてすごくおもしろそうだなぁと思いました。地球のこととかおもしろそう！Rubyでもそんなカンファレンスがあるとおもしろそう！

午前中に3時間のチュートリアルがあるのもおもしろかったです。Rubyのカンファレンスでもやりたいな。（すぐRubyのことを考えてしまう。）

午後はトークでした。知らないことばかりだったので非常に興味深かったです。Pythonではいろんなことが簡単にできるようになっていてワクワクしますね！Rubyでもそうなるといいな。

せっかくいろいろ知れたのでSciPy Japan Conference 2019の最中にこっそり[Optuna](https://github.com/pfnet/optuna)のRubyバインディングである[Red Optuna](https://github.com/red-data-tools/red-optuna)を作りました。SciPy Japan Conference 2019に参加してよかったです。Red Optunaのアイディアは初日のレセプションで佐野さんと話しているときにでてきたアイディアです。話せてよかったです。佐野さんは2日目に[Optunaのトーク](https://www.scipyjapan2019.scipy.org/talks-and-poster-schedule#comp-jtos9x27)をしました。

また参加したいと思ったカンファレンスでした。

### まとめ

SciPy Japan Conference 2019で日本のPythonユーザーのみなさんにApache Arrowを紹介しました。Rubyコミュニティー以外にもApache Arrowのことを紹介したいのでApache Arrowのことを知りたくなったら私に声をかけてください！

私は[データ処理ツールの開発という仕事をしたい]({% post_url 2018-07-11-index %})と思っています。その中にはもちろんApache Arrowの開発も含まれています。一緒に仕事をしたい！（自社サービスをApache Arrow対応したいとか）という方は[お問い合わせフォーム](/contact/?type=data-processing-tool)からご連絡ください。

---
tags:
- embedded
title: Node-REDをYoctoレシピによりインストールする
---
[2017年7月6日の記事]({% post_url 2017-07-06-index %})で紹介した通り、クリアコードは組み込みLinux向けにMozilla Firefoxを移植するプロジェクト[Gecko Embedded](https://github.com/webdino/gecko-embedded/wiki)を[WebDINO Japan（旧Mozilla Japan）](https://www.webdino.org/)様と共同で立ち上げ、開発を進めております。[Yocto](https://www.yoctoproject.org/)を使用してFirefoxをビルドしたりハードウェアクセラレーションを有効化する際のノウハウを蓄積して公開することで、同じ問題に悩む開発者の助けになることを目指しています。
<!--more-->


この記事では、Gecko Embedded本体ではなく周辺のミドルウェアの[Node-RED](https://nodered.org/)をRZ/G1で動かす話を書きます。
Node-REDは、ハードウェアデバイス/APIおよびオンラインサービスを接続するためのツールです。

### 現在のステータス

#### ターゲットボード

2月時点では、Node-REDのビルド及び動作は以下のボードで確認しています。

  * [iWave RainboW-G20D Q7](https://github.com/webdino/meta-browser/wiki/Build-RainboW-G20D-Q7-Yocto-2.0)

### ビルド方法

レシピはGitHubにて公開されているものを使用します。

  * https://github.com/imyller/meta-nodejs

  * https://github.com/imyller/meta-nodejs-contrib

Yoctoに組み込むには、meta-nodejs-contribを `git clone` したのち、(bitbakeのビルドディレクトリ)/conf/local.confへ

```conf
IMAGE_INSTALL_append = " node-red nodejs nodejs-npm "
PREFERRED_VERSION_nodejs = "6.11.2"
PREFERRED_VERSION_nodejs-native = "6.11.2"
```


(bitbakeのビルドディレクトリ)/conf/bblayers.confへ

```conf
BBLAYEYS += " ${TOPDIR}/../meta-nodejs "
BBLAYEYS += " ${TOPDIR}/../meta-nodejs-contrib "
```


をそれぞれ追加し、bitbakeを実行します。
ここで、RZ/G1向けのYoctoではNode.js 6.11.2の動作確認が取れているため、 `PREFERRED_VERSION_nodejs` には `6.11.2` を指定しています。

### 動作確認

Node-REDを上記の設定でビルドした場合、`node-red` コマンドが起動イメージにインストールされます。

```console
$ node-red
21 Feb 04:53:01 - [info] 

Welcome to Node-RED
===================

21 Feb 04:53:01 - [info] Node-RED version: v0.17.5
21 Feb 04:53:01 - [info] Node.js  version: v6.11.2
21 Feb 04:53:01 - [info] Linux 4.4.55-cip3 arm LE
21 Feb 04:53:07 - [info] Loading palette nodes
21 Feb 04:53:12 - [warn] ------------------------------------------------------
21 Feb 04:53:12 - [warn] [rpi-gpio] Info : Ignoring Raspberry Pi specific node
21 Feb 04:53:12 - [warn] ------------------------------------------------------
21 Feb 04:53:12 - [info] Settings file  : /home/root/.node-red/settings.js
21 Feb 04:53:12 - [info] User directory : /home/root/.node-red
21 Feb 04:53:12 - [info] Flows file     : /home/root/.node-red/flows_iwg20m.json
21 Feb 04:53:12 - [info] Creating new flow file
21 Feb 04:53:12 - [info] Starting flows
21 Feb 04:53:12 - [info] Started flows
21 Feb 04:53:12 - [info] Server now running at http://127.0.0.1:1880/
```


となれば動作確認は完了です。
Node-REDの使い方に関しては[Node-REDの公式サイトのドキュメント](https://nodered.org/docs/)を参照してください。

### まとめ

Node-REDのYoctoレシピを用いてRZ/Gシリーズのボードに載せた話を紹介しました。

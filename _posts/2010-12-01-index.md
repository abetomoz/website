---
tags:
- ruby
- presentation
title: "「全文検索エンジンgroongaを囲む夕べ #1」のRuby枠の資料公開"
---
先月の29日に、[全文検索エンジンgroongaを囲む夕べ #1](http://atnd.org/events/9234)が開催されました。内容は[groonga本体](http://groonga.org/)について、[groongaとRuby](http://groonga.rubyforge.org/)について、[groongaとMySQL](http://mroonga.github.com/)について、[groongaとPostgreSQL](http://textsearch-ja.projects.postgresql.org/textsearch_groonga.html)について、とgroonga三昧の内容でした。
<!--more-->


groongaとRubyについての資料は以降で紹介します。groongaとPostgreSQLについてはすでに資料が公開されています（[textsearch groonga v0.1](http://www.slideshare.net/ItagakiTakahiro/textsearch-groonga-v01)）。参加できなかった方は参考にしてください。

それでは、groongaとRubyについての資料を簡単な解説付きで紹介します。

[![Ruby loves groonga]({{ "/images/blog/20101201_0.png" | relative_url }} "Ruby loves groonga")](/archives/groonga-night-1/)

Ustreamで配信したものの録画もあります。Ruby枠は49分くらいからです。



### リリース情報

開催日当日の29日、groongaの新しいバージョン1.0.4がリリースされました。もちろん、この夕べに合わせたものです。

[![祝！groonga 1.0.4リリース！]({{ "/images/blog/20101201_1.png" | relative_url }} "祝！groonga 1.0.4リリース！")](/archives/groonga-night-1/ruby-loves-groonga-01.html)

さらに、groongaをRubyから使うためのライブラリrroongaも新しいバージョンがリリースされました。もちろん、これもこの夕べに合わせたものです。

[![祝！rroonga 1.0.4リリース！]({{ "/images/blog/20101201_2.png" | relative_url }} "祝！rroonga 1.0.4リリース！")](/archives/groonga-night-1/ruby-loves-groonga-02.html)

興味がある方はぜひ最新版を利用してください。

### 話すこと

[![話すこと]({{ "/images/blog/20101201_3.png" | relative_url }} "話すこと")](/archives/groonga-night-1/ruby-loves-groonga-03.html)

Rubyからgroongaを使う、ということが実に手になじむということを重点的に話します。これはgroongaをRubyらしく使えるライブラリが提供されているためです。そして、そのライブラリを提供しているのがラングバプロジェクトです。

### ラングバプロジェクト

[![ラングバプロジェクト]({{ "/images/blog/20101201_4.png" | relative_url }} "ラングバプロジェクト")](/archives/groonga-night-1/ruby-loves-groonga-15.html)

groongaをRubyらしく使えるライブラリや周辺ライブラリ・ツールなど、groongaとRubyに関連するソフトウェアなどを提供しているのが[ラングバプロジェクト](http://groonga.rubyforge.org/)です。WebサイトはRubyForgeにホスティングしてもらっていて、ソースコードは[GitHubにホスティング](https://github.com/ranguba/)してもらっています。以降で紹介するライブラリ・ツールはすべてGitHub上にあります。

[![ラングバプロジェクト]({{ "/images/blog/20101201_5.png" | relative_url }} "ラングバプロジェクト")](/archives/groonga-night-1/ruby-loves-groonga-16.html)

ラングバプロジェクトの目標は「*使いやすい検索システムを手早く、簡単に*」実現できるようにすることです。

[![提供物]({{ "/images/blog/20101201_6.png" | relative_url }} "提供物")](/archives/groonga-night-1/ruby-loves-groonga-17.html)

そのために、ラングバプロジェクトは全文検索システムを提供します。しかし、1つの全文検索システムですべてのケースをカバーすることはできません。そのため、全文検索システムを作るために必要な部品も再利用できる形で提供します。部品を組み合わせることで環境に合わせた全文検索システムを構築することができます。

このようにして「*使いやすい検索システムを手早く、簡単に*」という目標の実現を目指します。

### 提供物一覧

まずは、ラングバプロジェクトがどのような名前のライブラリ・ツールを提供しているかを紹介します。それぞれのライブラリ・ツールが全文検索システムのどの部分の機能を提供するかは図で示します。

[![全文検索システム]({{ "/images/blog/20101201_7.png" | relative_url }} "全文検索システム")](/archives/groonga-night-1/ruby-loves-groonga-19.html)

まず、全文検索システムにはどのような機能があるかを確認しておきましょう。

全文検索システムは、まず、「検索対象」から文書を収集します。これが「クローラー」と呼ばれる機能です。次に、収集してきた文書からテキスト情報やタイトル・更新時刻などといったメタ情報を抽出し、全文検索エンジンに登録します。この文書を登録する機能を「インデクサー」と呼びます。最後に、全文検索エンジンに登録されている文書を簡単・便利にユーザが検索できるインターフェイスが必要です。これが「検索インターフェイス」です。最近はWebアプリケーションとして「検索インターフェイス」を提供することが多いです。

それでは、ラングバプロジェクトが提供する機能の紹介です。

[![rroonga]({{ "/images/blog/20101201_8.png" | relative_url }} "rroonga")](/archives/groonga-night-1/ruby-loves-groonga-21.html)

全文検索エンジン[groonga](http://groonga.org/)（ぐるんが）をRubyから利用するためのライブラリである[rroonga](http://groonga.rubyforge.org/#about-rroonga)（るるんが）を提供します。

[![ChupaText]({{ "/images/blog/20101201_9.png" | relative_url }} "ChupaText")](/archives/groonga-night-1/ruby-loves-groonga-22.html)

インデクサーで必要になる、文書からテキストとメタ情報を抽出するライブラリ・ツールが[ChupaText](http://groonga.rubyforge.org/#about-chupatext)（ちゅぱてきすと）です。

[![ChupaRuby]({{ "/images/blog/20101201_10.png" | relative_url }} "ChupaRuby")](/archives/groonga-night-1/ruby-loves-groonga-23.html)

ChupaTextをRubyのライブラリとして使えるようにするのが[ChupaRuby](http://groonga.rubyforge.org/#about-chuparuby)（ちゅぱるびー）です。

[![ActiveGroonga]({{ "/images/blog/20101201_11.png" | relative_url }} "ActiveGroonga")](/archives/groonga-night-1/ruby-loves-groonga-24.html)

rroongaをRuby on Rails 3から簡単に使えるようにするのが[ActiveGroonga](http://groonga.rubyforge.org/#about-active-groonga)（あくてぃぶぐるんが）です。

[![racknga]({{ "/images/blog/20101201_12.png" | relative_url }} "racknga")](/archives/groonga-night-1/ruby-loves-groonga-25.html)

Rails 3も含む[Rack](http://rack.rubyforge.org/)アプリケーションで使えるユーティリティを集めたのが[racknga](http://groonga.rubyforge.org/#about-racknga)（らくんが）です。

[![文書検索ラングバ]({{ "/images/blog/20101201_13.png" | relative_url }} "文書検索ラングバ")](/archives/groonga-night-1/ruby-loves-groonga-26.html)

これらのライブラリ・ツールを利用した全文検索システムが[文書検索ラングバ](http://groonga.rubyforge.org/#about-ranguba)です。プロジェクトが提供するライブラリ・ツールを集結したものなので、プロジェクトと同じ名前が付いています。

それでは、それぞれのツールについて紹介します。

### rroonga

[![roonga]({{ "/images/blog/20101201_14.png" | relative_url }} "roonga")](/archives/groonga-night-1/ruby-loves-groonga-34.html)

rronngaはRubyからgroongaの全文検索エンジンの機能を使えるようにするためのライブラリです。groongaの機能をRubyらしい書き方で書けることを重視したAPIになっていることが特徴です。

[![スキーマ定義]({{ "/images/blog/20101201_15.png" | relative_url }} "スキーマ定義")](/archives/groonga-night-1/ruby-loves-groonga-38.html)

例えば、groongaのスキーマを言語内[DSL](http://ja.wikipedia.org/wiki/%E3%83%89%E3%83%A1%E3%82%A4%E3%83%B3%E5%9B%BA%E6%9C%89%E8%A8%80%E8%AA%9E)として書けるようになっています。groonga自体もスキーマを定義するための言語を持っていますが、rroongaではこのように通常のRubyの式としてスキーマを書けるようにしています。Rubyでスキーマを書けるため、新しくgroongaのスキーマ定義用の文法を覚える必要がありません。

[![クエリ]({{ "/images/blog/20101201_16.png" | relative_url }} "クエリ")](/archives/groonga-night-1/ruby-loves-groonga-41.html)

検索条件も通常のRubyの式で書けます。Rubyでは「=~」は「マッチさせる」場合に使われる演算子ですが、rroongaでも「マッチ（キーワードを全文検索してキーワードが含まれる文書を返す）」という意味で使っています。自然な書き方ですね。

[![クエリ: 複雑]({{ "/images/blog/20101201_17.png" | relative_url }} "クエリ: 複雑")](/archives/groonga-night-1/ruby-loves-groonga-42.html)

もう少し複雑な条件も自然に書くことができます。例では「タイトルか説明から本文に"Ruby"と"検索"という単語を含んでいる1ヶ月以内のサイト」を検索しています[^0]。この条件でもRubyで使われている演算子を同じ意味で使っているので、自然に書くことができます。

検索条件をRubyで指定すると検索が遅くなるのではと思うかもしれません。しかし、その心配は不要です。ブロックの中身は1度だけ評価され、groongaの条件式オブジェクトにコンパイルされます。groongaはそのC言語で記述された条件式オブジェクトを使って検索を行うので、rroongaを使わない場合と同様に高速に検索します。

ORマッパーでは検索条件はSQL文字列を組み立てることに相当しますが、rroongaでは直接groongaの条件式を作ります。検索エンジンがSQL文字列をパースする必要がないため、より少ないコストで検索のための下準備をすることができます。

[![利用シーン]({{ "/images/blog/20101201_18.png" | relative_url }} "利用シーン")](/archives/groonga-night-1/ruby-loves-groonga-46.html)

rroongaは様々な場面で有用です。例えば、データを加工しながらgroongaに登録するインデクサーを作る場合です。サイト検索システムなら各ページで共通のヘッダー部分とフッター部分は検索対象から外したいでしょう。そのとき、Rubyでそのあたりの処理を実装してそのままgroongaに登録できると便利です。

他にも、groongaに入っているデータを利用しながら新しいデータを登録する場合もrroongaを使うと便利です。

[![利用例]({{ "/images/blog/20101201_19.png" | relative_url }} "利用例")](/archives/groonga-night-1/ruby-loves-groonga-47.html)

rroongaは[るりまサーチ](http://rurema.clear-code.com/)でも利用されているので、実際にどのように使われているかを調べたいときは、[るりまサーチのソースコード](http://github.com/kou/rurema-search)を見てください。

rroongaは以下のコマンドでインストールできます。

{% raw %}
```
% sudo gem install rroonga
```
{% endraw %}

groongaがインストールされていない場合は自動的にダウンロード・ビルドして利用します。便利ですね。

### ChupaText

[![ChupaText]({{ "/images/blog/20101201_20.png" | relative_url }} "ChupaText")](/archives/groonga-night-1/ruby-loves-groonga-48.html)

[ChupaText](http://groonga.rubyforge.org/#about-chupatext)はテキスト抽出ツールです。「複数の入力形式を1つの操作方法」で扱えることを目標に開発が進められています。

ChupaTextはchupatextコマンドとC言語用のAPIを提供しています。chupatextコマンドは抽出したテキストを[Multipurpose_Internet_Mail_Extensions](https://ja.wikipedia.org/wiki/Multipurpose_Internet_Mail_Extensions)形式で出力します。MIME形式を採用したのは以下の理由からです。

  * 広く普及している形式である。
  * メタデータとテキストを扱える。
    * メタデータはヘッダーとして表現。ヘッダーの項目は拡張可能なこともちょうどよい。
    * テキストは本文として表現。
  * 複数のファイルを扱える。アーカイブからテキストを抽出した
    場合にこれがうれしくなる。

[![拡張性]({{ "/images/blog/20101201_21.png" | relative_url }} "拡張性")](/archives/groonga-night-1/ruby-loves-groonga-53.html)

入力形式はこれからも増えていくでしょう。それに対応するため、ChupaTextは拡張しやすい設計になっています。ChupaText本体を変更しなくても新しい入力形式に対応するためのモジュールを後付けで追加することができます。

現在はC言語で共有ライブラリを作成するか、Rubyスクリプトを作成して、所定の位置に置くことで入力形式を追加することができます。Rubyスクリプトでも拡張できるのは、ChupaText内にRubyインタプリタが組み込まれているからです。

ChupaTextはaptitudeでインストールできます。

{% raw %}
```
% sudo aptitude install chupatext
```
{% endraw %}

詳細は[ChupaTextのインストールドキュメント](http://groonga.rubyforge.org/chupatext/ja/install.html)を見てください。

### ChupaRuby

[![ChupaRuby]({{ "/images/blog/20101201_22.png" | relative_url }} "ChupaRuby")](/archives/groonga-night-1/ruby-loves-groonga-55.html)

[ChupaRuby](http://groonga.rubyforge.org/#about-chuparuby)はChupaTextをRubyのライブラリとして使用できるようにします。ChupaTextをHTTP経由で利用するためのインターフェイスを追加する予定です。これが追加されると、「テキスト抽出Webサービス」を構築することができるようになります。

ChupaRubyは以下でインストールすることができます。

{% raw %}
```
% sudo gem install chuparuby
```
{% endraw %}

ChupaTextは自動的にインストールされないので、事前にインストールしておいてください。

### ActiveGroonga

[![ActiveGroonga]({{ "/images/blog/20101201_23.png" | relative_url }} "ActiveGroonga")](/archives/groonga-night-1/ruby-loves-groonga-58.html)

[ActiveGroonga](http://groonga.rubyforge.org/#about-active-groonga)はRails 3用のモデルライブラリです。groongaをモデルのデータストアとして利用することができます。ActiveRecordが提供しているような以下の機能を提供します。

  * ActiveGroonga::Base（ActiveRecord::Base相当）
  * rake groonga:*（rake db:*相当）
  * ジェネレーション
  * マイグレーション
  * バリデーション
  * リレーション

ActiveGroongaは以下でインストールすることができます。

{% raw %}
```
% sudo gem install activegroonga
```
{% endraw %}

ActiveGroongaに関するより詳しいことは別の機会に紹介します。

### racknga

[![racknga]({{ "/images/blog/20101201_24.png" | relative_url }} "racknga")](/archives/groonga-night-1/ruby-loves-groonga-63.html)

[racknga](http://groonga.rubyforge.org/#about-racknga)は上記のパッケージに入れるには適切ではないな、というものが詰まっているユーティリティパッケージです。Rackのミドルウェアや[Passenger用Muninプラグイン]({% post_url 2010-06-14-index %})などがここに入っています。

rackngaは以下でインストールすることができます。

{% raw %}
```
% sudo gem install racknga
```
{% endraw %}

### 文書検索ラングバ

[![文書検索ラングバ]({{ "/images/blog/20101201_25.png" | relative_url }} "文書検索ラングバ")](/archives/groonga-night-1/ruby-loves-groonga-66.html)

[文書検索ラングバ](http://groonga.rubyforge.org/#about-ranguba)はこれまで紹介したライブラリ・ツールを利用した全文検索システムです。groongaの特徴を活かしたWebベースの検索インターフェイスを提供します。例えば、るりまサーチでも提供しているドリルダウン機能などです。

文書検索ラングバはそろそろリリースされる予定です。もうしばらくお待ちください。

### まとめ

groongaのRuby関連の情報を紹介しました。便利なライブラリやツールが充実していっているので、ぜひ、Rubyとgroongaを使って全文検索システムを作ってみてください。

[![お知らせ]({{ "/images/blog/20101201_26.png" | relative_url }} "お知らせ")](/archives/groonga-night-1/ruby-loves-groonga-69.html)

[^0]: この資料を使ったのは2010年11月29日です。

---
tags: []
title: SourceForge.netからOSDNへ移行する方法
---
以前、[OSDNのファイルリリース機能をAPI経由で使うには]({% post_url 2016-04-05-index %})という記事で機能を紹介したきりになっていましたが、[milter manager](https://milter-manager.osdn.jp/)というプロジェクトで全面的にSourceForge.netからOSDNに移行したので備忘録を兼ねて記事にします。
<!--more-->


移行の理由は以下の通りです。

  * SourceForge.netの度重なる仕様変更に追従するのが大変だった

    * ファイルのダウンロードリンクが変わってもリダイレクトもされないので辛かった

  * SourceForge.netのミラーが減った

    * ミラー先にファイルがないことがある

    * リダイレクト先が国内にサーバーがあるのに海外のサーバーになることがある

  * SourceForge.net運営による乗っ取りやファイル改竄が複数あった

milter managerは最初のバージョンをリリースした2009年から7年間SourceForge.netにお世話になってきました。SourceForge.netがなかったらmilter managerの開発にはもっと手間と費用がかかっていたはずです。SourceForge.netには大変感謝しています。

前提として、ソースコードリポジトリは既にGitHubに移行済みとします[^0]。

### SourceForge.net で使用していた機能

| 機能                  | 移行可否 | 備考                                                                                                                    |
|-----------------------|----------|-------------------------------------------------------------------------------------------------------------------------|
| メーリングリスト      | :ok:     | 英語のメーリングリストを作るときは、管理画面から言語設定を変更する。データの移行はできない。                            |
| フォーラム            | :ok:     | データの移行はできない。ほとんど使用されていなかった                                                                    |
| Webサイトホスティング | :ok:     | rsyncが使えるのでそのままのディレクトリ構造で丸ごと移行できる。SourceForge.net側に.htaccessでリダイレクト設定ができる。 |
| ファイルリリース      | :ok:     | [自動化もできた](https://github.com/clear-code/cutter/blob/master/misc/release-to-osdn.rb)                              |
| ニュース              | :ok:     | なし                                                                                                                    |
| apt/yumリポジトリ     | :ng:     | OSDNにはapt/yumをホストできる機能がないので[packagecloud](https://packagecloud.io)に移行することにした                  |

機能的にはほとんどの部分で代替できることがわかったので、移行することにしました。

メーリングリストは以下の4つを作成していました。

  * milter-manager-users-ja: 日本語のわかる人向け

  * milter-manager-users-en: 英語のわかる人向け

  * milter-manager-commit: コミットメール用

  * milter-manager-bugs: バグ報告用

milter-manager-bugsはほとんど使用されていなかったので移行しないことに決めました。それ以外の3つはOSDNでメーリングリストを作成しました。このときmilter-manager-users-enは英語のわかる人向けなので[入会用ページ](http://lists.osdn.me/mailman/listinfo/milter-manager-users-en)の表示言語を英語にしました。なお、メーリングリストに登録されているメンバーは数が少なかったので手動で移行しました。

Webサイトのホスティングは問題なくできました。rsyncを使ってSourceForge.netのときと同じディレクトリ構成でアップロードすることができました。
SourceForge.net側で.htaccessにリダイレクトの設定をすることができるので、301リダイレクトの設定をしておきました。

```
Redirect permanent / https://milter-manager.osdn.jp/
```


ファイルリリースの自動化については[以前の記事]({% post_url 2016-04-05-index %})を参照してください。

SourceForge.netのときもニュース機能を使用していましたが、この記事を書くまで存在を忘れていました[^1]。

### packagecloudについて

[OSDNにはapt/yumリポジトリを置けない問題]({% post_url 2016-04-05-index %})があるのでpackagecloudにapt/yumリポジトリについてはpackagecloudに移行することにしました。

[packagecloud](https://packagecloud.io)ではRubyGems,deb,RPMなどいくつかのパッケージを配布することができます。[価格](https://packagecloud.io/pricing)を見るとオープンソースのプロジェクトだと25GBの容量を無料で使えることがわかりました。[milter manager](https://packagecloud.io/milter-manager)で使用するために申し込んだところ、ほぼ即日使えるようになりました。無料で使わせてもらうかわりに、[Assets and Brand Guidelines](https://packagecloud.io/brand-guidelines)にある画像を使ってリンクを張る必要がありました。

パッケージのアップロードもコマンド一発で簡単にできました。

一つだけ問題があってDebian GNU/Linuxのsid(unstable)向けのパッケージを配布できるようにはなっていませんでした。理由をサポートに問い合わせたところ

> sidは日々パッケージ構成が変わるのでpackagecloud側で動作環境を保証できないため提供していない


とのことでした[^2]。この件については、milter managerをなるべく早くDebianのオフィシャルパッケージにすることで対応しようと考えています。なお、サポートの返信はとても早く対応もとても丁寧でした。

[Launchpad](https://launchpad.net/)などと違ってローカルでパッケージをビルドする必要がありますが、ビルドしたパッケージの動作確認をしてからアップロードできるという利点があります。パッケージのビルド部分はそのままで、アップロード部分のみの変更で済んだのもmilter managerプロジェクトとしては利点でした。Launchpadのようにビルドサーバーも提供しているようなサービスの場合、必要なファイルをアップロードしてからパッケージをビルドするため、ビルドに失敗したり、ビルドしたパッケージに問題があった場合に再アップロードが必要となるため修正するのに時間がかかるという欠点があります。

### 具体的にやったこと

  1. OSDNにプロジェクト作成

  1. packagecloudをOSSプランで使えるように手続き

  1. 古いファイル(deb/rpm)をpackagecloudにアップロード

  1. ファイルリリースに置いていた古いファイルをOSDNにアップロード

  1. メーリングリスト開設・設定変更

  1. メーリングリストメンバー移行

  1. SourceForge.netに置く .htaccess のテスト

  1. ビルドスクリプトの修正

  1. ドキュメントの修正

  1. 移行と新しいバージョンのリリース

### まとめ

他の作業と並行して進めていたので、2ヶ月弱くらいかかりましたが特にハマることなく移行できました。
メーリングリストや rsync によるウェブサイトホスティング等、GitHub や Bitbucket 等にはない便利な機能があるのでSourceForge.netからの移行先としてOSDNを検討してみるのはいかがでしょうか。

[^0]: OSDNではGit以外にMercurial,Subversion,Bazaarが使えるようです

[^1]: リリース時はスクリプトによる自動投稿だったため

[^2]: 現時点で次の次のstable(Debian 10)になる予定のbuster向けのパッケージは置けるようになっていました

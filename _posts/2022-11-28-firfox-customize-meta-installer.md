---
title: "Firefoxのメタインストーラーをカスタマイズ・更新する方法"
author: kenhys
tags:
  - mozilla
---

### はじめに

クリアコードでは、[Firefoxサポートサービス]({% link services/mozilla/menu.html %})の一環として、お客さまからの要望に基づいてカスタマイズしたFirefox(メタインストーラー)の提供もしています。
その際、Firefoxをお客様自身でもカスタマイズもしくは更新できるように、メタインストーラーの生成ツールもあわせて提供しています。

Firefox ESRのマイナーバージョンアップの際には、更新方法に関するお問い合わせをいただくこともあるので、
今回はメタインストーラーの作成キットを用いたカスタマイズ・更新する方法を説明します。

<!--more-->

### メタインストーラーについて

Mozillaの提供しているFirefoxのインストーラーを実行し、その後カスタマイズした設定ファイルなどをまとめて適用するのがFirefoxのメタインストーラーです。

#### メタインストーラー生成ツールの役割

メタインストーラーの役割は、顧客の情報システム用の設定ファイルを取り込んだFirefoxのインストーラーを作成することです。
このメタインストーラーを利用することで、組織内のすべての端末に対して一律に設定を反映することが可能になり、運用効率の向上を図ることができます。[^cm]

[^cm]: 作成されたメタインストーラーの配布自体は、別途構成管理ツール等で実施されることが多いです。

#### メタインストーラー生成ツールの構成

メタインストーラーの全てのファイルは「FxInstaller-*」という名前のフォルダに格納されます。
このフォルダの内容は以下の通りです。(顧客に合わせてフォルダにプレフィクスをつけるなど、名前は変更していることがあります)

```
FxMetaInstaller-*/
├── 7zr.exe	圧縮解凍ツール（7zip Standalone Console）
├── fainstall.exe	メタインストーラの素材となる実行ファイル
├── fainstall.ini	fainstall.exeの挙動を制御するための設定ファイル
├── fainstall.sfx	自己解凍ファイルを生成するための素材
├── pack.list	圧縮対象のファイルのリスト
├── FxMetaInstaller.bat	メタインストーラを生成するバッチファイル
└── resources
        ├── autoconfig.js	集中管理を有効化するためのファイル
        ├── autoconfig.cfg	集中管理設定の参照先を制御する
        ├── Firefox Setup 102.X.0esr.exe	Firefox本体のインストーラー
        ├── Firefox-setup.ini	Firefox本体のインストーラを制御するファイル
        └── policies.json	ポリシー設定を有効化するためのファイル
```


### メタインストーラーの基本的な使い方

#### メタインストーラーの作成

メタインストーラーを作成する作業は、バッチファイルにより自動化されています。
運用担当者が行わなければならない作業は次のとおりです。

1. メタインストーラーのフォルダ（FxMetaInstaller-*）を作業用環境のローカルディスク上に配置する

※ この時、フォルダがファイル共有サーバ上のUNCパス「`\\（サーバ名）\（パス）`」で表される位置にあると以降の作業を行えません。

![メタインストーラーをデスクトップに配置した画面]({% link images/blog/firefox-customize-meta-installer/firefox-put-source.png %})

2. メタインストーラーのフォルダを開く

![メタインストーラーのフォルダを開いた画面]({% link images/blog/firefox-customize-meta-installer/firefox-open-source-directory.png %})

3. メタインストーラーを構成するファイルがすべて揃っていることを確認する

![resourcesフォルダを開いた画面]({% link images/blog/firefox-customize-meta-installer/firefox-ensure-resources.png %})

4. FxMetaInstaller.batを実行する

![fxmetainstaller.batを実行した画面]({% link images/blog/firefox-customize-meta-installer/firefox-execute-bat.png %})

以上の手順により、FxMetaInstaller-102.3.0.exeという名前のメタインストーラーが生成されます。

![Firefox 102.3.0.exeが作成された画面]({% link images/blog/firefox-customize-meta-installer/firefox-102.3-installer.png %})

#### メタインストーラーによるFirefoxのインストール

各端末に Firefox をインストールする手順は、以下の通りです。

1. 生成したメタインストーラーを端末に配置する
2. 端末上で、管理者権限でメタインストーラーを実行する

メタインストーラーの構成によっては、ユーザーの操作を必要としないインストール状況を示すダイアログが表示されることがあります。

#### Firefoxのバージョン確認方法

Firefoxのバージョンを確認方法は次のとおりです。

1. Firefoxを起動する
2. メニューの「ヘルプを表示」-「Firefoxについて」をクリックする

![Mozilla Firefoxについて表示したときの画面]({% link images/blog/firefox-customize-meta-installer/firefox-help-about-mozilla.png %})

#### メタインストーラーによるFirefoxのアンインストール

各端末からFirefox をアンインストールする手順は、以下の通りです。

1. 設定>アプリ>インストールされているアプリを開く

![インストールされているアプリの一覧画面]({% link images/blog/firefox-customize-meta-installer/firefox-installed-apps.png %})

2. Fx Meta Installerを選択し、「アンインストール」をクリックする

![アプリと関連情報の削除の確認画面]({% link images/blog/firefox-customize-meta-installer/firefox-uninstall.png %})

3. アプリと関連情報の削除の確認画面で「アンインストール」をクリックする

![アンインストール済みの画面]({% link images/blog/firefox-customize-meta-installer/firefox-uninstall-confirm.png %})

Fx Meta Installerを削除すると、同時にMozilla Firefoxもアンインストールされます。

4. インストールされているアプリ一覧にMozilla Firefox ESRが存在しないことを確認する

![アンインストール済みの画面]({% link images/blog/firefox-customize-meta-installer/firefox-confirm-uninstalled.png %})

5. ユーザーのプロファイルの削除

ユーザの Firefox プロファイル削除は、前回使用時の設定情報などの影響を除いて完全にクリーンな状態で検証を行いたい場合や、ハードディスクの空き容量を可能な限り確保したい場合などに行います。

なお、ユーザの Firefox プロファイルを削除すると、ブックマークや履歴など、そのプロファイル内にのみ保存されている個人的な情報はすべて失われることに注意が必要です。
以上を踏まえて、ユーザの Firefox プロファイルを削除する場合は、以下の各ディレクトリを削除します。

* %HOMEPATH%\AppData\Roaming\Mozilla\Firefox (%APPDATA%/Mozilla/Firefox)

![Roaming配下のプロファイルの画面]({% link images/blog/firefox-customize-meta-installer/firefox-appdata-firefox.png %})

* %HOMEPATH%\AppData\Local\Mozilla\Firefox (%LOCALAPPDATA%/Mozilla/Firefox)

![Local配下のプロファイルの画面]({% link images/blog/firefox-customize-meta-installer/firefox-localappdata-firefox.png %})

### メタインストーラーのカスタマイズ

#### Firefoxのバージョンを更新する

Firefoxのバージョンを更新する場合は、次の手順でメタインストーラーを再作成します。

以下では、102.3.0から102.4.0に更新する場合を例に説明を行います。

1.ブラウザで https://releases.mozilla.org/pub/firefox/releases/ を開く
 
![releases.mozilla.orgにアクセスしたときの画面]({% link images/blog/firefox-customize-meta-installer/firefox-releases.png %})


2.利用したいバージョン「102.4.0esr」を選択し、続いてwin64 > ja を選択する
（例：「https://releases.mozilla.org/pub/firefox/releases/102.4.0esr/win64/ja/」）

![releases.mozilla.orgにアクセスしたときの画面]({% link images/blog/firefox-customize-meta-installer/firefox-ja.png %})

(32bit版を利用する場合は win32 > jaを選択します。)

3.実行ファイル（EXE）形式とMSI形式のインストーラーの２つのうち、実行ファイル形式の物をダウンロードし、メタインストーラーのresources/以下に配置する

![exeをresourcdesに配置した画面]({% link images/blog/firefox-customize-meta-installer/firefox-copy-102.4esr.png %})

4.旧バージョンのFirefoxのインストーラー（Firefox Setup 102.3.0esr.exe）をresources/から削除する

![旧バージョンを削除した画面]({% link images/blog/firefox-customize-meta-installer/firefox-replace-with-102.4esr.png %})

5.テキストエディタでfainstall.iniを開き、次の３つの設定値を更新対象のバージョンに書き換える
  
変更前：

```
AppMinVersion=102.3.0
AppMaxVersion=102.3.0
DisplayVersion=102.3.0
```

変更後：

```
AppMinVersion=102.4.0
AppMaxVersion=102.4.0
DisplayVersion=102.4.0
```

`DisplayVersion`はメタインストーラーのバージョンです。特に理由がなければFirefoxのバージョンと揃えておくと、あとからわかりやすいのでおすすめです。

6. FxMetaInstaller.batを実行する
 
以上の手順により、更新後のバージョン番号のファイル名（FxMetaInstaller-102.4.0.exe）で新しいメタインストーラが生成されます。
 
![102.4.0のインストーラーを作成した画面]({% link images/blog/firefox-customize-meta-installer/firefox-102.4-installer.png %})

#### Firefoxのカスタマイズ設定を変更する

Firefox のカスタマイズ情報は、原則として「resources/autoconfig.cfg」と「resources/policies.json」に格納されています。[^jsc]

[^jsc]: リモートファイルを参照する場合には、上記のほかにresources/autoconfig.jscなどが含まれている場合もあります。

「resources/autoconfig.cfg」は JavaScript の書式で記述でき、次の関数を通じて、Firefox の設定情報を変更できます。

* pref(key, value): 設定項目の設定値を変更する
* lockPref(key, value): 設定項目の設定値を変更し、かつユーザによる変更を禁止する
* defaultPref(key, value): 設定項目の既定値のみを変更する（ユーザ設定は維持する）

「resources/policies.json」は JSON の書式で記述でき、あらかじめ決まった名前のキーの値を通じて、Firefoxの設定情報を管理できます。[^policy]

[^policy]: [Mozilla Firefox ESR60でのPolicy Engineによるポリシー設定の方法と設定項目のまとめ]({% post_url 2018-05-12-index %})で設定例を解説しています。



設定を変更したメタインストーラーを作成する手順は、以下の通りです。

1. テキストエディタで「resources/autoconfig.cfg」および「resources/policies.json」を開き、編集する
2. FxMetaInstaller.bat を実行する

以上の手順により、更新後の設定ファイルを同梱した新しいメタインストーラーが生成されます。

#### インストーラー実行時の挙動をカスタマイズする

編集することで、メタインストーラーを実行した際の様々な挙動をカスタマイズできます。


以下に、代表的な設定項目とその設定例を示します。

##### 「resources/Firefox-setup.ini」の設定

Firefox-setup.iniはFirefox 本体のインストーラを制御するための設定ファイルです。

|項目名|説明|設定例|
|--|--|--|
|install > DesktopShortcut| デスクトップ上にショートカットを作成するか|[install]<br>DesktopShortcut=true|
|install > StartMenuShortcuts| スタートメニューにショートカットを作成するか|[install]<br>StartMenuShortcuts=false|
|install > TaskbarShortcut| タスクバーにショートカットを作成するか|[install]<br>TaskbarShortcut=false|

##### 「fainstall.ini」の設定

fainstall.iniはメタインストーラーの独自機能を制御するための設定ファイルです。

|項目名|説明| 設定例|
|--|--|--|
|fainstall > AppEnableCrashReport| クラッシュレポートの送信を許可するか|[fainstall]<br>AppEnableCrashReport=false|
|fainstall > RequireAdminPrivilege| インストール実行時に管理者権限の確認を行うか|[fainstall]<br>RequireAdminPrivilege=false|

設定を変更したメタインストーラーを作成する手順は以下のとおりです。

1. テキストエディタで「resources/Firefox-setup.ini」ないし「fainstall.ini」を開き、編集する
2. FxMetaInstaller.bat を実行する

以上の手順により、更新後の設定ファイルを同梱した新しいメタインストーラーが生成されます。

### おわりに

今回はメタインストーラーの作成キットを用いたFirefoxのカスタマイズや、Firefoxのマイナーアップデートにともなうメタインストーラーを更新する方法を説明しました。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお問い合わせください。
  

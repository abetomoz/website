---
tags: []
title: Debian SourcesのAPIを使ってパッチが数多く適用されているパッケージを調べるには
---
### はじめに

今年もAdvent Calendarの季節になりましたね。
<!--more-->


[Debsources now in sources.debian.org](https://bits.debian.org/2017/12/debsources-now-in-sources-debian-org.html)と題したメールにて、`https://sources.debian.org/` というサイトの公開がアナウンスされました。

従来 `sources.debian.net` ドメインで稼働していたものが、Debian公式として提供されるようになりました。

Debian Sourcesについては、さっそく解説している[記事](https://qiita.com/knok/items/cfd698ae4cba7e1fe036)もあります。

今回は、Debian Sourcesにて提供されているAPIの使用例として、よく使われているパッケージにどれくらいパッチが当てられているのかを調べてみることにします。

API に関しては [API Documentation](https://sources.debian.org/doc/api/) があるのでそちらを参考にしました。APIにアクセスするために特別な認証は必要ありません。

#### パッケージリストを取得する

パッケージリストについては `https://sources.debian.org/api/list` からJSONのレスポンスを取得することができます。

`prefix` つきでパッケージリストを取得することもできるようです。

#### よく使われているパッケージを調べる

よく使われているパッケージについては、 [Debian Popularity Contest](https://popcon.debian.org/)の結果を利用しました。

例えば、 `main` カテゴリなら `https://popcon.debian.org/main/by_inst` にアクセスするとどれだけインストールされたかという値を取得できます。

今回は取得したパッケージリストをすべて調べ上げることはせず、10000回以上インストールされているパッケージのみをフィルタする目的で使いました。

#### パッケージごとにパッチのリストを取得

パッチファイルのリストは `https://sources.debian.org/patches/api/(パッケージ名)/latest` でJSONのレスポンスを取得できます。

例えば、 `systemd` のパッチリストを取得するなら `https://sources.debian.org/patches/api/systemd/latest` にアクセスします。

#### パッチ適用状況のランキング

2017年12月18日現在のパッチ適用状況を元にしています。
`main` カテゴリのうち、popconで10000回以上インストールされており、パッチの適用数が多いもの上位50件リストアップした結果は以下のとおりです。

No.|Package|Patch
-----|-----|-----
1|systemd|59
2|imagemagick|56
3|libxml2|55
4|php5|52
5|gnupg2|51
6|cups|50
7|bash|50
8|php7.0|47
9|mutt|46
10|perl|46
11|thunderbird|42
12|python3.5|38
13|w3m|37
14|ppp|36
15|python3.6|35
16|firefox-esr|33
17|libreoffice|31
18|iceweasel|31
19|openssl|30
20|procmail|30
21|wireshark|28
22|bsd-mailx|27
23|ispell|26
24|fortune-mod|26
25|cinnamon|25
26|texlive-base|23
27|festival|22
28|ntp|21
29|network-manager|20
30|pm-utils|20
31|blt|20
32|util-linux|20
33|transfig|19
34|groff|19
35|sysvinit|18
36|ruby1.9.1|17
37|cpio|17
38|samba|17
39|pulseaudio|17
40|rpm|17
41|open-vm-tools|17
42|parted|16
43|arj|16
44|nodejs|16
45|clamav|16
46|xsane|16
47|tmux|15
48|libsoftware-license-perl|15
49|exim4|15
50|tcpdump|15

### まとめ

今回は、`Debian Sources` というサイトのAPIの使い方を紹介しました。
単純なAPIですが、他の情報と組み合わせると「よく使われているパッケージのうち、パッチの適用数が多いもの」をリストアップしてみたりすることもできます。

この記事を書いている時点では、まだパッチに関して[DEP3](http://dep.debian.net/deps/dep3/)の情報をレスポンスに含めるようにはなっていないようです。[^0]

DEP3の情報が含まれていると、「Forwarded」という「このパッチはアップストリームに報告されているか？」といった情報を調べて、まだ報告されていないなら報告しよう、といったことがやりやすくなります。[^1]

今後そういった情報も含まれるともっと活用の幅が広がるかもしれません。

[^0]: DEP3の情報が含まれていたら、まだアップストリームに報告していないパッチをたくさん抱えているパッケージをリストアップすることができた。

[^1]: sources.debian.org の各パッケージの debian/patches フォルダを確認するのが手っ取り早そうです。

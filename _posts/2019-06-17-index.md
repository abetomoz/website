---
tags:
  - apache-arrow
title: 代表取締役の須藤がApache Arrowの開発でGoogle Open Source Peer Bonusを受賞
---
Apache Arrowの開発に参加している代表取締役の須藤です。
<!--more-->


もう2ヶ月前の話なのですが、[2019年のGoogle Open Source Peer Bonusの1人に選ばれました](https://opensource.googleblog.com/2019/04/google-open-source-peer-bonus-winners.html)。選ばれるまでこんな取り組みがあることを知らなかったのですが、2011年からやっているそうです。オープンソースの開発に関わっている人を支援する取り組みだそうです。賞金（？）として250ドルもらいました。ありがとうございます。

Apache Arrowの開発をがんばっているということで選んでもらいました。現時点で私は[2番目にコミット数が多い](https://github.com/apache/arrow/graphs/contributors)のでそのあたりを評価してくれたのかもしれません。ちなみに、1番コミット数が多いWesさんはpandasの開発で選ばれていました。

私の自由なソフトウェアの開発を支援したいけどどう支援したらよいかわからないという方はご相談ください。

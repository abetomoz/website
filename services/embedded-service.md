---
layout: default
main_title: 組み込み
sub_title:
type: service
---

クリアコードでは、各種の組み込みLinux機器向けの様々なソフトウェア開発にこれまで関わってきました。

特定の用途や機能を必要とすることの多い組み込みシステムの分野において、さまざまな実績とソフトウェアに関する知識で、既存技術の組み合わせや、新規開発などお客様のニーズに合わせた開発を行っています。開発した機能などは、できる限りフリーソフトウェアとして公開しています。

以下の記事で、これまでに対応してきた組み込み技術関連のソフトウェア開発について、知見を紹介しています。


## Gecko/Webkit等レンダリングエンジン関連

 * [Gecko Embeddedプロジェクト]({% post_url 2017-07-06-index %})
 * [Gecko Embedded 次期ESR対応]({% post_url 2018-03-30-index %})
 * [apitraceを使ったOpenGL ESが絡むFirefoxのデバッグ方法]({% post_url 2018-06-15-index %})
 * [apitraceを使ったfirefoxのWebGLのデバッグ例]({% post_url 2018-07-02-index %})
 * [Gecko Embedded 68ESR対応]({% post_url 2020-04-22-index %})


## Yocto等組み込みLinuxディストリビューション関連

* [ChainerのYoctoレシピを公開]({% post_url 2017-11-15-index %})
* [YoctoでFcitxをビルドする]({% post_url 2018-11-08-index %})
* [YoctoのWeston上で日本語入力]({% post_url 2018-08-31-index %})
* [Node-REDをYoctoレシピによりインストールする]({% post_url 2018-02-22-index %})
* [PC上でWeston + Wayland版Chromium + Fcitx5での日本語入力環境を構築する]({% post_url 2021-11-24-setup-env-for-embedded-jp-software-keyboard-dev %})
* [YoctoのWestonをターゲットとしたFcitx5ベースの仮想キーボードの開発]({% post_url 2022-12-02-fcitx5-virtualkeyboard-ui %})

### Fcitx5ベースの仮想キーボード　デモビデオ

<iframe width="560" height="315" src="https://www.youtube.com/embed/f1QBqWy_Ps4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Gstreamer等マルチメディアフレームワーク関連

* [GStreamerのエレメントをつないでパイプラインを組み立てるには]({% post_url 2014-07-22-index %})
* [あしたのオープンソース研究所: GStreamer]({% post_url 2010-01-11-index %})


## サービスに関する問い合わせ

サービスに関するお問い合わせは、こちらの [お問い合わせフォーム](/contact/) からご連絡ください。

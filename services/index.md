---
layout: default
main_title: サービス
sub_title:
type: service
description: Groonga/Fluentd/Apache Arrowをはじめとしたソフトウェアの開発・技術支援をソースコードレベルで提供します。2006年からフリーソフトウェアを開発している確かな技術力でお客様の要望にあわせた開発・カスタマイズ・導入支援・コンサルティングをします。
---


クリアコードはフリーソフトウェア関連のサービスを提供します。既存フリーソフトウェアを活用した開発あるいは既存フリーソフトウェアのカスタマイズ・導入支援・運用支援などをします。
<h2 id='browserselector'>BrowserSelector</h2>

URLパターンに応じて起動するブラウザを切り替えるBrowserSelectorを開発しました。
BrowserSelectorはInternet Explorer、Microsoft Edge、Chrome、Firefoxといった主要なブラウザを切り替えて運用したい場合に最適です。例えば社内イントラアクセス、特定のシステム、外部インターネット利用などで利用したいブラウザを分けたい場合に、自動で適切なブラウザが起動します。

お客様の運用状況に合わせて、カスタマイズや設定をサポートできます。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='browserselector.html'>ブラウザ切替ツール BrowserSelector</a>
</ul>

<h2 id='fluentd'>Fluentd</h2>

これまで、日本国内において航空会社、官公庁、通信事業者をはじめとした大規模ユーザーに対する支援を行ってきました。
Fluentd/Fluent Bitのサポート、導入支援、プラグイン開発などを行います。

また、Fluentd/Fluent Bitと各種プラグインのメンテナンスのほか、コミュニティでの様々な活動も行っています。たとえば英語の公式リリースノートを日本語で紹介しています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='fluentd.html'>Fluentdに関するよくある質問集</a>
  <li class='cell medium-6 large-4'><a href='fluentd-service.html'>Fluentdサポートサービス</a>
  <li class='cell medium-6 large-4'><a href='fluent-bit.html'>Fluent Bit のご紹介</a>
</ul>

<h2 id='groonga'>Groonga</h2>

クリアコードは国産全文検索エンジンGroongaプロジェクトのメンバーとして10年以上Groongaおよび関連プロダクトを開発しています。

Groonga/PGroonga/Mroongaの機能拡張、サポート、導入支援を行います。新規導入のほか、既存システムの改善などのご相談も対応可能です。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='groonga.html'>Groongaサポートサービス</a>
</ul>

<h2 id='data-processing-tool'>データ処理ツール開発</h2>

モダンなデータ処理ツールの基盤として利用されているApache Arrowを活用したデータ処理の効率化を支援します。Apache Arrowは様々な言語で利用することができるデータ処理高速化技術なので、言語に依存せずに効率化できます。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='apache-arrow.html'>Apache Arrowのご紹介</a>
</ul>

<h2 id='browser-customize'>ブラウザカスタマイズ</h2>

クリアコードでは専用端末向けのブラウザカスタマイズや、拡張機能の開発など、各種ブラウザに関するご相談を承っています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='browser-customize.html'>ブラウザカスタマイズ</a>
</ul>

<h2 id='mozilla'>Firefox/Thunderbird</h2>

FirefoxやThunderbirdの企業導入支援として、カスタマイズ、アドオン開発、サポートを提供します。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='mozilla/menu.html'>Firefox/Thunderbirdサポートサービス</a>
</ul>

<h2 id= 'embedded-service'>組み込み機器ソフトウェア開発</h2>

家電製品や、機械の制御パネルなどで多く使われる組み込み機器向けのソフトウェア開発に関して、ニーズに合わせた設計、開発、サポートなどを提供しています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='embedded-service.html'>組み込み機器ソフトウェア開発</a>
</ul>

<h2 id='oss-development-support'>OSS開発支援サービス</h2>

OSSを開発すること・OSSの開発に参加することにメリットを持つ企業向けのクリアコードの開発支援サービスです。それぞれの企業の事情を確認したうえで、目標に合わせて、必要なサポートを提供します。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='oss-development-support.html'>OSS開発支援サービス</a>
</ul>

<h2 id='code-reader'>開発チームの支援</h2>

フリーソフトウェア開発の経験を活用して、開発チームを対象としたワークショップ/研修会を行っています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='code-reader'>コードリーダー育成支援</a>
</ul>

<h2 id='milter-manager'>メールシステム(milter manager)</h2>

迷惑メール対策システムの構築を簡単・低コストにする迷惑メール対策ソフトウェア「milter manager」のサポートやmilterの開発を行います。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='milter-manager.html'>milter managerサービスメニュー一覧</a>
</ul>
